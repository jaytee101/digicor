# Digicor Configurator

[![Build status](https://jaytwitch.visualstudio.com/clients/_apis/build/status/digicor-configurator%20Release)](https://jaytwitch.visualstudio.com/clients/_build/latest?definitionId=3)

This is a system configurator based off [Thinkmate](https://thinkmate.com) and [Broadberry](https://broadberry.co.uk) sites - which allows digicor's customers to
create a quote for themselves.

## Contributing

Gitflow workflow is used here

There are two main branches - develop & master
-- [master](https://bitbucket.org/jaytee101/digicor/src/master/) is the version which gets deployed to the production server (in this case, digicor.jtiong.com until finished)
-- [develop](https://bitbucket.org/jaytee101/digicor/src/develop/) is the branch you base your local development environment off. This will always be more up to date than master branch
