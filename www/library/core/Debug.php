<?php

namespace app\Core;

use \R as R;

class Debug
{
    public static function print($data, $die = false)
    {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        if ($die) die();
    }
}
