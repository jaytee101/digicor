<?php

namespace app\Core;

use \R as R;

class Log
{
    public static function access($action, $location = null, $line = null)
    {
        $log = R::xdispense('access_logs');
        $log->time       = time();
        $log->action     = $action;
        $log->ip_address = $_SERVER['REMOTE_ADDR'];
        $log->user       = !empty($_SESSION['auth']) ? $_SESSION['user']['id'] : null;
        $log->location   = !empty($location) ? $location : null;
        $log->line       = !empty($line) ? $line : null;
        R::store($log);
    }

    public static function activity($action, $origin = '')
    {
        $log = R::xdispense('action_logs');
        $log->time       = time();
        $log->origin     = $origin;
        $log->action     = $action;
        $log->ip_address = $_SERVER['REMOTE_ADDR'];
        $log->user       = !empty($_SESSION['auth']) ? $_SESSION['user']['id'] : null;
        R::store($log);
    }

    public static function error($action, $line = null, $origin = null)
    {
        $log = R::xdispense('error_logs');
        $log->time       = time();
        $log->origin     = $origin;
        $log->line       = $line;
        $log->action     = $action;
        $log->ip_address = $_SERVER['REMOTE_ADDR'];
        $log->user       = !empty($_SESSION['auth']) ? $_SESSION['user']['id'] : null;
        R::store($log);
    }
}
