<?php

require ('../config.php');

$uploadFolder = __DIR__ . '/../uploads/';
$onlinePath = APP_URL . '/uploads/';

$response = array();

try {
    if (empty($_FILES['file'])) {
        throw new Exception('no file to upload');
    }

    // adjust filename
    $filename = uniqid() . '.' . (pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION) ? : 'png');

    // move file into uploaded folder
    move_uploaded_file($_FILES['file']['tmp_name'], $uploadFolder . $filename);
    $response['filename'] = $onlinePath . $filename;

    if (!file_exists($uploadFolder . $filename)) {
        throw new Exception('could not move file to uploads folder');
    }
} catch (Exception $e) {
    echo "Error: {$e->getMessage()}";
}

echo json_encode($response);