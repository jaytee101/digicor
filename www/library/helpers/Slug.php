<?php

namespace app\Helpers;

use \R as R;

class Slug
{
    public static function generate($text)
    {
        if (empty($text)) {
            return null;
        }

        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        $text = trim($text, '-');
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = strtolower($text);
        $text = preg_replace('~[^-\w]+~', '', $text);

        return $text;
    }
}
