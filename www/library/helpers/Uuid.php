<?php

namespace app\Helpers;

use \R as R;

class Uuid
{
    public static function generate($email, $timestamp)
    {
        if (empty($email)) {
            return null;
        }

        $t = explode(" ", microtime());
        return sprintf( '%05s-%08s-%08s-%04s',
            substr("00000".dechex(random_int(10000, 99999)),-5),        // random id number
            substr(md5($email), -8),                                    // user's email
            substr("00000000".dechex($timestamp), -8),                  // unixtime the user joined at
            substr("0000".dechex(round($t[0]*65536)),-4));              // microtime code
    }
}
