<?php

namespace app\Helpers;

use \app\Core\Debug as Debug;
use \app\Helpers\Alert as Alert;

use \R as R;

class File
{
    function upload()
    {
        $target_dir    = ROOT . "/uploads/";
        $target_file   = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk      = true;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if($check !== false) {
                $uploadOk = true;
            } else {
                Alert::create('danger', 'This file is not an image file.');
                $uploadOk = false;
            }
        }

        // Check if file already exists
        if (file_exists($target_file)) {
            Alert::create('danger', 'Sorry, a file with this name already exists.');
            $uploadOk = false;
        }

        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            Alert::create('danger', 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.');
            $uploadOk = false;
        }

        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 1) {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                $alertOpts = [
                    'recipient' => $_SESSION['user']['id'],
                    'message'   => "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.",
                    'style'     => 'success'
                ];

                $data = [];
                $filename = explode("_", $_FILES["fileToUpload"]["name"]);
                $data['retailer'] = $filename[0];
                $data['date_extracted'] = $filename[3];

                // date extracted needs to be rearranged
                $ext = str_split($data['date_extracted']);
                $new = [];
                $new[0] = $ext[4];
                $new[1] = $ext[5];
                $new[2] = $ext[6];
                $new[3] = $ext[7];
                $new[4] = $ext[0];
                $new[5] = $ext[1];
                $new[6] = $ext[2];
                $new[7] = $ext[3];

                $dateExtracted = implode("", $new);

                $screen = R::dispense('screenshots');
                $screen['uploaded']  = time();
                $screen['extracted'] = $dateExtracted;
                $screen['week']      = date('W', strtotime($dateExtracted));
                $screen['filename']  = $_FILES["fileToUpload"]["name"];
                $screen['retailer']  = $filename[0];
                $screen['uploader']  = $_SESSION['user']['id'];
                $screen['status']    = 'pending';
                R::store($screen);
            } else {
                $alertOpts = [
                    'recipient' => $_SESSION['user']['id'],
                    'message'   => "Sorry, there was an error uploading your file.",
                    'style'     => 'danger'
                ];
            }
        }

        Alert::create($alertOpts);
    }
}
