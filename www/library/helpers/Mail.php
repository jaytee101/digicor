<?php

namespace app\Helpers;

use \PHPMailer as PHPMailer;
use \R as R;

class Mail
{
    public static function send($email, $subject, $body)
    {
        $address = isset($_SERVER['HTTP_CF_CONNECTING_IP']) ? $_SERVER['HTTP_CF_CONNECTING_IP'] : $_SERVER['REMOTE_ADDR'];

        $mail = new PHPMailer;
        $mail->isSMTP();
        // $mail->SMTPDebug  = 2; // verbose debugging
        $mail->Host       = 'tls://smtp.gmail.com';
        $mail->SMTPAuth   = true;
        $mail->Username   = 'j.tiong.notifier@gmail.com';
        $mail->Password   = 'Icewind.Neverwinter!19850306';
        $mail->SMTPSecure = 'tls';
        $mail->Port       = 587;
        $mail->setFrom('j.tiong.notifier@gmail.com', 'jtiong.com (No-Reply)');
        $mail->addAddress($email);
        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $body;

        if (!$mail->send()) {
            Log::create('Contact form failed to send mail to Sarah');
            Alert::create('danger', 'Could not send email! The following error has occured:<br /><br />' . $mail->ErrorInfo);

            return false;
        } else {
            Log::create('Contact form has sent mail to Sarah successfully from: ' . $address);
            Alert::create('success', 'Your mail has been sent! We will be in touch! Thank you!');

            return true;
        }
    }
}
