<?php

switch ($_SERVER['HTTP_HOST']) {
    //
    // development
    //
    case 'digicor.local':
        define('ENVIRONMENT', 'development');
        define('DB_TYPE', 'mysql');
        define('DB_HOST', 'digicor_db');
        define('DB_PORT', '3306');
        define('DB_NAME', 'digicor');
        define('DB_USER', 'root');
        define('DB_PASS', 'D!g1c0r_');

        define('TIMEZONE', 'Australia/Sydney');

        define('APP_NAME', 'Digicor Configurator');
        define('APP_URL', 'http://digicor.local');

        define('COOKIE_NAME', 'digicor.configurator');

        define('ADMIN_EMAIL', 'admin@digicor.com.au');
        define('ADMIN_PASSWORD', 'password');
        break;

    //
    // production
    //
    case 'digicor.jtiong.com':
        define('ENVIRONMENT', 'production');
        define('DB_TYPE', 'mysql');
        define('DB_HOST', 'digicor_db');
        define('DB_PORT', '3306');
        define('DB_NAME', 'digicor');
        define('DB_USER', 'root');
        define('DB_PASS', 'D!g1c0r_');

        define('TIMEZONE', 'Australia/Sydney');

        define('APP_NAME', 'Digicor Configurator');
        define('APP_URL', 'https://digicor.jtiong.com');

        define('COOKIE_NAME', 'digicor.configurator');

        define('ADMIN_EMAIL', 'admin@digicor.com.au');
        define('ADMIN_PASSWORD', 'password');
        break;
}
