<?php

foreach (glob(ROOT."/routes/*.php") as $filename) {
    include $filename;
}

// ERROR: 404
$base->notFound(function () {
    $error = new \app\Core\Error;
    $error->display('404');
});
