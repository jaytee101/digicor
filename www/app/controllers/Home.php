<?php

namespace app\Controllers;

use \app\Core\Controller as Controller;
use \app\Core\Log as Log;
use \app\Helpers\Alert as Alert;
use \app\Models\Auth as Auth;
use \app\Models\User as User;

use \R as R;

class Home extends Controller
{
    public function index()
    {
        if (!empty($_POST['action'])) {
            switch ($_POST['action']) {
                case 'create':
                    $create = User::create();

                    if ($create == true) {
                        Alert::create('success', 'Your account has been created, ' . $_POST['firstname'] . '! You can now login');
                        Log::activity('New User account has been created (' . $_POST['email'] . ')');
                    }
                    break;

                case 'login':
                    Auth::verify();
                    break;

                case 'logout':
                    $_SESSION = [];
                    session_regenerate_id();
                    session_destroy();

                    if (isset($_COOKIE[COOKIE_NAME])) {
                        $sessions = R::find('users_sessions', ' user = ?', [ $_POST['user'] ]);

                        foreach ($sessions as $s) {
                            R::trash($s);
                        }
                        setcookie(COOKIE_NAME, '', time()-42000, '/');
                    }

                    Alert::create('danger', 'You are now logged out! Bye! See you next time!');
                    break;

                case 'setCurrency':
                    if ($_POST['currency'] == 'NZD') {
                        $geodata = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?base_currency=AUD&ip=122.56.237.1')); // 122.56.237.1 - ns1.xtra.co.nz
                        $_SESSION['user']['location'] = [
                            'country'    => $geodata['geoplugin_countryCode'],
                            'conversion' => $geodata['geoplugin_currencyConverter']
                        ];
                    } else {
                        $geodata = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?base_currency=AUD&ip=203.173.10.217')); // 203.173.10.217 - 203-173-10-217.perm.iinet.net.au
                        $_SESSION['user']['location'] = [
                            'country'    => $geodata['geoplugin_countryCode'],
                            'conversion' => $geodata['geoplugin_currencyConverter']
                        ];
                    }

                    header('Location:' . $_POST['redirect']);
                    break;
            }
        }

        $this->viewOpts['page']['title']   = 'Digicor Configurator Prototype';
        $this->viewOpts['page']['content'] = 'home/index';
        $this->viewOpts['menu']['enabled'] = true;
        $this->viewOpts['menu']['content'] = 'main';
        $this->viewOpts['menu']['section'] = 'home';

        $this->viewData['configurations'] = R::find('configurations', ' enabled = 1 AND published IS NOT NULL');

        $this->view->load($this->viewOpts, $this->viewData);
    }

    public function changelog()
    {
        // git log command:
        // git log --pretty=format:'<commit>%h|%an|%s|%d|%cr' --abbrev-commit --date=relative
        $gitlog = file_get_contents(ROOT . '/gitlog');
        $gitlog = explode('<commit>', $gitlog);

        $changelog = [];
        $commitLog = [];

        foreach ($gitlog as $commit) {
            if (empty($commit)) {
                continue;
            }

            $commit = explode('|', $commit);

            $data = [];
            $data['hash']    = $commit[0];
            $data['author']  = $commit[1];
            $data['message'] = $commit[2];
            $data['description'] = '';
            for ($i = 3; $i < (count($commit) - 2); $i++) {
                if (!empty($commit[$i])) {
                    $data['description'] .= $commit[$i];
                }
            }
            $data['version'] = !empty($commit[count($commit) - 2]) ? substr($commit[count($commit) - 2], 2, -1) : $commit[count($commit) - 2];
            $data['time']    = end($commit);

            array_push($changelog, $data);
        }

        foreach ($changelog as $commit) {
            if (!empty($commit['time'])) {
                // vet words from versioning
                $versionWords = [
                    ' ', ',', '->', '/',
                    'origin',
                    'master',
                    'develop',
                    'HEAD',
                    'tag:',
                ];

                foreach ($versionWords as $vW) {
                    $commit['version'] = str_replace($vW, '', $commit['version']);
                }

                $commitLog[date('d/m/Y', (int)$commit['time'])][] = [
                    'hash'        => $commit['hash'],
                    'message'     => $commit['message'],
                    'description' => !empty($commit['description']) ? $commit['description']: '',
                    'version'     => !empty($commit['version']) ? $commit['version'] : '',
                ];
            }
        }

        $this->viewOpts['page']['fullwidth'] = false;
        $this->viewOpts['page']['title']     = 'Digicor Configurator Dev Notes';
        $this->viewOpts['page']['content']   = 'home/changelog';

        $this->viewOpts['menu']['enabled'] = true;
        $this->viewOpts['menu']['content'] = 'main';
        $this->viewOpts['menu']['section'] = 'home';

        $this->viewData['commitLog'] = $commitLog;

        $this->view->load($this->viewOpts, $this->viewData);
    }
}
