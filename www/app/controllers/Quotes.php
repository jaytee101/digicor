<?php
namespace app\Controllers;

use \app\Core\Controller as Controller;
use \app\Core\Log as Log;
use \app\Helpers\Alert as Alert;
use \app\Helpers\File as File;
use \app\Helpers\Time as Time;

use \app\Models\Category as Category;
use \app\Models\Component as Component;
use \app\Models\Configuration as Configuration;

use \R as R;

class Quotes extends Controller
{
    public function view($id = 0)
    {
        if (empty($id)) {
            Alert::create('danger', 'No config ID provided');
            header("Location:/");
        }

        if (!empty($_POST)) {
            switch ($_POST['action']) {
                case 'ajax':
                    switch ($_POST['call']) {
                        case 'cartUpdate':
                            $this->cartUpdate();
                            exit();
                            break;
                    }
                    break;
            }
        }

        $config = R::load('configurations', $id);

        if (empty($config)) {
            Alert::create('danger', 'No config data available for the provided ID');
            header("Location:/");
        }

        $this->viewOpts['page']['title']   = 'Get a Quote | ' . $config->name;
        $this->viewOpts['page']['content'] = 'quotes/view';

        $this->viewOpts['menu']['enabled'] = true;
        $this->viewOpts['menu']['content'] = 'main';
        $this->viewOpts['menu']['section'] = 'home';

        $this->viewData['components'] = R::find('components', ' hidden = 0 AND enabled = 1 ORDER BY name ASC');
        $this->viewData['categories'] = R::find('categories', ' visible = 1 AND enabled = 1 ORDER BY name ASC');

        // configuration specific
        $this->viewData['config'] = $config;
        $this->viewData['sections'] = R::find('config_sections', ' configuration = ? AND enabled = 1', [ $config->id ]);

        $this->view->load($this->viewOpts, $this->viewData);
    }

    /**
     * update quote cart via ajax request
     *
     * @return void
     */
    public function cartUpdate()
    {
        $data = $_POST['data'];

        // declaring HTML source containers
        $data['cartList'] = '';

        // get configuration information
        $config = R::load('configurations', $data['configId']);
        $conversionRate = empty($_SESSION['user']['location']['conversion']) ? 1 : $_SESSION['user']['location']['conversion'];

        $total = $config->price;

        // get the unique sections from a config
        $sections = R::find('config_sections', ' configuration = ? AND enabled = 1', [ $data['configId'] ]);
        foreach ($sections as $section) {
            $sectionLimits[$section->id]['itemlimit'] = $section->itemlimit;
            $sectionLimits[$section->id]['count'] = 0;
        }

        // hidden products
        $hiddenComponents = R::find('config_components', ' configuration =  ? AND hidden = 1 AND enabled = 1', [ $data['configId'] ]);

        // visible cart products
        foreach ($data['cart'] as $product) {
            $confComponent = R::load('config_components', $product['name']);

            // reset components per product
            $comp  = R::load('components', $confComponent->component);
            $qty = 0;
            $price = 0;

            // product unselected == 0
            if ($product['value'] > 0) {
                $qty   = $product['value'];
                $price = $qty * $comp->price;
                $total += $price;

                $sectionLimits[$confComponent->section]['count'] += $qty;
            }

            // item count
            $data['itemcounts']['section-' . $confComponent->section . '-count'] = $sectionLimits[$confComponent->section]['count'];

            // update visible list
            if ($qty != 0) {
                $data['cartList'] .= '<tr><td>' . $qty . 'x</td><td>' . $comp->name . '</td></tr>';
            }
        }

        // total price
        $data['totalprice'] = $total * $conversionRate;

        if (ENVIRONMENT == 'development') {
            $data['environment'] = 'development';
            $data['testOutputHTML']  = '<pre>' . print_r($data, true) . '</pre>';
        }

        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }
}
