<?php

namespace app\Controllers;

use \app\Core\Controller as Controller;
use \app\Core\Log as Log;
use \app\Helpers\Alert as Alert;
use \app\Helpers\File as File;
use \app\Helpers\Time as Time;

use \app\Models\Category as Category;

use \R as R;

class Categories extends Controller
{
    public function admin()
    {
        if (empty($_SESSION['authenticated'])) {
            header("Location:/");
        }

        if (!empty($_POST['action'])) {
            switch ($_POST['action']) {
                case 'create':
                    Category::create();
                    break;

                case 'show':
                    Category::show($_POST['category']);
                    break;

                case 'hide':
                    Category::hide($_POST['category']);
                    break;

                case 'disable':
                    Category::disable($_POST['category']);
                    break;

                case 'enable':
                    Category::enable($_POST['category']);
                    break;

                case 'delete':
                    Category::delete($_POST['category']);
                    break;

                case 'update':
                    Category::update($_POST['category']);
                    break;
            }
        }

        $this->viewOpts['page']['title']   = 'DC Admin | Categories';
        $this->viewOpts['page']['content'] = 'categories/admin';

        $this->viewOpts['menu']['enabled'] = true;
        $this->viewOpts['menu']['content'] = 'admin';
        $this->viewOpts['menu']['section'] = 'categories';

        $this->viewOpts['sidebar']['enabled'] = true;
        $this->viewOpts['sidebar']['content'] = 'categories';
        $this->viewOpts['sidebar']['section'] = 'home';

        $this->viewOpts['footer']['enabled'] = false;

        $this->viewData['categories'] = R::find('categories');

        $this->view->load($this->viewOpts, $this->viewData);
    }
}
