<?php

namespace app\Controllers;

use \app\Core\Controller as Controller;
use \app\Core\Log as Log;
use \app\Helpers\Alert as Alert;
use \app\Helpers\File as File;
use \app\Helpers\Slug as Slug;
use \app\Helpers\Time as Time;

use \app\Models\Category as Category;
use \app\Models\Component as Component;

use \R as R;

class Components extends Controller
{
    public function admin($disabled = false)
    {
        if (empty($_SESSION['authenticated'])) {
            header("Location:/");
        }

        if (!empty($_POST['action'])) {
            switch ($_POST['action']) {
                case 'create':
                    Component::create($_POST);
                    Alert::create('success', 'New component created: ' . $_POST['name'] . ' (' . $_POST['itemcode'] . ')');
                    break;

                case 'edit':
                    Component::update($_POST['component']);
                    break;

                case 'disable':
                    Component::disable($_POST['component']);
                    break;

                case 'enable':
                    Component::enable($_POST['component']);
                    break;
            }
        }

        $this->viewOpts['page']['title']   = 'DC Admin | Components';
        $this->viewOpts['page']['content'] = 'components/admin';

        $this->viewOpts['menu']['enabled'] = true;
        $this->viewOpts['menu']['content'] = 'admin';
        $this->viewOpts['menu']['section'] = 'components';

        $this->viewOpts['sidebar']['enabled'] = true;
        $this->viewOpts['sidebar']['content'] = 'components';
        $this->viewOpts['sidebar']['section'] = 'home';

        $this->viewOpts['footer']['enabled'] = false;

        $this->viewData['categories'] = R::find('categories', ' enabled = 1 ORDER BY name ASC'); // only enabled categories
        $this->viewData['components'] = R::find('components', ' enabled = 1');
        $this->viewData['disabled']   = false;

        if ($disabled == true) {
            $this->viewOpts['page']['content'] = 'components/disabled';

            $this->viewData['components'] = R::find('components', ' enabled = 0');
            $this->viewData['disabled']   = true;
        }

        $this->view->load($this->viewOpts, $this->viewData);
    }

    public function byCategory($id = 0)
    {
        if (empty($_SESSION['authenticated'])) {
            header("Location:/");
        }

        $this->viewOpts['page']['title']   = 'DC Admin | Components';
        $this->viewOpts['page']['content'] = 'components/admin';

        $this->viewOpts['menu']['enabled'] = true;
        $this->viewOpts['menu']['content'] = 'admin';
        $this->viewOpts['menu']['section'] = 'components';

        $this->viewOpts['sidebar']['enabled'] = true;
        $this->viewOpts['sidebar']['content'] = 'components';
        $this->viewOpts['sidebar']['section'] = 'home';

        $this->viewOpts['footer']['enabled'] = false;

        $this->viewData['categories'] = R::find('categories', ' enabled = 1 ORDER BY name ASC');
        $this->viewData['components'] = R::find('components', ' category_id = ? AND enabled = 1', [ $id ]);
        $this->viewData['currentCat'] = R::load('categories', $id);

        $this->view->load($this->viewOpts, $this->viewData);
    }

    public function create()
    {
        if (empty($_SESSION['authenticated'])) {
            header("Location:/");
        }

        $this->viewOpts['page']['title']   = 'DC Admin | Components';
        $this->viewOpts['page']['content'] = 'components/create';

        $this->viewOpts['menu']['enabled'] = true;
        $this->viewOpts['menu']['content'] = 'admin';
        $this->viewOpts['menu']['section'] = 'components';

        $this->viewOpts['sidebar']['enabled'] = true;
        $this->viewOpts['sidebar']['content'] = 'components';
        $this->viewOpts['sidebar']['section'] = 'create';

        $this->viewOpts['footer']['enabled'] = false;

        $this->viewData['categories'] = R::find('categories', ' enabled = 1 ORDER BY name ASC'); // only enabled categories
        $this->view->load($this->viewOpts, $this->viewData);
    }

    public static function ajaxView($id = 0)
    {
        $component = R::load('components', $id);

        // output the HTML form fields for editing a component
        ?>
        <div class="form-group row">
            <label for="category" class="col-sm-2 col-form-label">Category</label>
            <div class="col-sm-4">
                <input type="text" id="category" class="form-control flexdatalist" list="categories" data-min-length="1" name="category" value="<?=Category::get($component->category_id, 'name')?>">
            </div>
        </div>
        <div class="form-group row">
            <label for="price" class="col-sm-2 col-form-label">Price</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="price" name="price" value="<?=$component->price?>">
                <small class="form-text text-muted">Price in cents (e.g. $99.20 = 9920)</small>
            </div>
        </div>
        <div class="form-group row">
            <label for="itemcode" class="col-sm-2 col-form-label">SKU</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="itemcode" name="itemcode" value="<?=$component->itemcode?>">
            </div>
        </div>
        <div class="form-group row">
            <label for="itemcode" class="col-sm-2 col-form-label">Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="name" name="name" value="<?=htmlentities($component->name)?>">
            </div>
        </div>
        <div class="form-group row">
            <label for="description" class="col-sm-2 col-form-label">Description</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="description" name="description" value="<?=htmlentities($component->description)?>">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-2 col-form-label">Image</div>
            <div class="col-sm-10">
                <?php
                if (!empty($component->image)) {
                    ?>
                    <img src="<?=$component->image?>" alt="" style="height: 250px; " />
                    <br />
                    <br />
                    <?php
                }
                ?>
                <input type="file" name="image" id="" class="form-control-file">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-2 col-form-label">Clear image</div>
            <div class="col-sm-1">
                <div class="form-check">
                    <input type="hidden" name="removeImg" value="0">
                    <input type="checkbox" name="removeImg" id="removeImg" class="form-check-input" value="1">
                    <label for="removeImg" class="form-check-label">Yes</label>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-2 col-form-label">Hide Price</div>
            <div class="col-sm-1">
                <div class="form-check">
                    <input type="hidden" name="hidden" value="0">
                    <input type="checkbox" name="hidden" id="hiddenPrice" class="form-check-input" value="1">
                    <label for="hiddenPrice" class="form-check-label">Yes</label>
                </div>
            </div>
        </div>

        <input type="hidden" name="action" value="edit">
        <input type="hidden" name="component" value="<?=$component->id?>">
        <input type="hidden" name="componentCategory" value="<?=$component->category?>">
        <?php
    }

    public static function ajaxCategoryComponents($id = 0)
    {
        $components = R::find('components', ' category_id = ? AND enabled = 1', [ $id ]);
        $componentCount = $_REQUEST['count'];

        $response = [];

        // html of selection row
        $response['html'] = '
        <tr class="row" id="component-' . $componentCount . '">
            <td class="col-sm-4">
                <select name="components[' . $componentCount . '][id]" class="form-control componentSelector">
                    <option value="">Select a component...</option>';

        foreach ($components as $component) {
            $response['html'] .= '<option value="' . $component->id . '" data-price="' . $component->price . '" data-row="' . $componentCount . '">' . $component->name . '</option>';
        }

        $response['html'] .= '
                </select>
            </td>
            <td class="col">
                <input type="text" class="form-control" name="components[' . $componentCount . '][price]" placeholder="Price" id="price-' . $componentCount . '" />
            </td>
            <td class="col">
                <div class="form-check" style="padding-left: 2rem; ">
                    <input type="hidden" name="components[' . $componentCount . '][pricehide]" value="0">
                    <input type="checkbox" name="components[' . $componentCount . '][pricehide]" id="hiddenPrice-' . $componentCount . '" class="form-check-input" value="1">
                    <label for="hiddenPrice-' . $componentCount . '" class="form-check-label">Hide</label>
                </div>
            </td>
            <td class="col">
                <input type="text" class="form-control" name="components[' . $componentCount . '][change]" placeholder="Change" />
            </td>
            <td class="col">
                <div class="form-check" style="padding-left: 2rem; ">
                <input type="hidden" name="components[' . $componentCount . '][changehide]" value="0">
                <input type="checkbox" name="components[' . $componentCount . '][changehide]" id="hiddenChange-' . $componentCount . '" class="form-check-input" value="1">
                <label for="hiddenChange-' . $componentCount . '" class="form-check-label">Hide</label>
                </div>
            </td>
            <td class="col">
                <input type="text" class="form-control" name="components[' . $componentCount . '][baseqty]" placeholder="Base Qty" />
            </td>
            <td class="col">
                <input type="text" class="form-control" name="components[' . $componentCount . '][maxqty]" placeholder="Max Qty" />
            </td>
            <td class="col">
                <input type="text" class="form-control" name="components[' . $componentCount . '][groupqty]" placeholder="Group Qty" />
            </td>
            <td class="col">
                <input type="hidden" name="components[' . $componentCount . '][category]" value="' . $component->category_id . '" />
                <button type="button" class="btn btn-danger removeComponent" data-id="' . $componentCount . '"><i class="fa fa-times-circle"></i></button>
            </td>
        </tr>';

        $response['counter'] = $componentCount + 1;
        $response['category'] = $id;

        echo json_encode($response);
    }

    public function import()
    {
        if (empty($_SESSION['authenticated'])) {
            header("Location:/");
        }

        $importing = false;

        $this->viewOpts['page']['content'] = 'components/import';

        if (!empty($_POST)) {
            switch ($_POST['action']) {
                case 'import':
                    foreach ($_POST['components'] as $component) {
                        Component::create($component);
                    }
                    break;

                case 'upload':
                    try {
                        $uploaddir = ROOT . '/uploads/';
                        $filename  = $uploaddir . time() . '_' . Slug::generate(basename($_FILES['upload']['name'])) . '.' . pathinfo($_FILES['upload']['name'], PATHINFO_EXTENSION);

                        if (move_uploaded_file($_FILES['upload']['tmp_name'], $filename)) {
                            $this->viewOpts['page']['content'] = 'components/importing';

                            $this->viewData['categories']  = R::find('categories', ' enabled = 1');
                            $this->viewData['oldFilename'] = basename($_FILES['upload']['name']);

                            if (pathinfo($_FILES['upload']['name'], PATHINFO_EXTENSION) == 'xlsx' || 'xls') {
                                $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader(ucfirst(strtolower(pathinfo($_FILES['upload']['name'], PATHINFO_EXTENSION))));
                                $spreadsheet = $reader->load($filename);
                                $data = $spreadsheet->getActiveSheet()->toArray();

                                $picData = [];

                                $i = 0; // image count
                                foreach ($spreadsheet->getActiveSheet()->getDrawingCollection() as $drawing) {
                                    if ($drawing instanceof \PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing) {
                                        ob_start();
                                        call_user_func(
                                            $drawing->getRenderingFunction(),
                                            $drawing->getImageResource()
                                        );
                                        $imageContents = ob_get_contents();
                                        ob_end_clean();

                                        switch ($drawing->getMimeType()) {
                                            case \PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing::MIMETYPE_PNG :
                                                $extension = 'png';
                                                break;
                                            case \PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing::MIMETYPE_GIF:
                                                $extension = 'gif';
                                                break;
                                            case \PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing::MIMETYPE_JPEG :
                                                $extension = 'jpg';
                                                break;
                                        }
                                    } else {
                                        $zipReader = fopen($drawing->getPath(), 'r');
                                        $imageContents = '';
                                        while (!feof($zipReader)) {
                                            $imageContents .= fread($zipReader, 1024);
                                        }
                                        fclose($zipReader);
                                        $extension = $drawing->getExtension();
                                    }

                                    $myFileName = 'images/' . date('Y-m-d-H-i-s') . '_img_' . $i . '.' . $extension;
                                    file_put_contents($uploaddir . $myFileName, $imageContents);

                                    $picData[$i]['coordinates'] = $drawing->getCoordinates();
                                    $picData[$i]['filename'] = $myFileName;
                                    $i++;
                                }

                                // merging picData with data
                                foreach ($picData as $pic) {
                                    preg_match('/[^\d]+/', $pic['coordinates'], $col);
                                    preg_match('/\d+/', $pic['coordinates'], $row);
                                    $col = $col[0];
                                    $row = $row[0];

                                    $data[$row - 1][8] = $pic['filename'];
                                }

                                $this->viewData['dataArray'] = $data;
                            } else if (pathinfo($_FILES['upload']['name'], PATHINFO_EXTENSION) == 'csv') {
                                $this->viewData['dataArray'] = array_map('str_getcsv', file($filename, FILE_IGNORE_NEW_LINES));

                                Alert::create('success', 'File successfully uploaded: ' . basename($_FILES['upload']['name']));

                                if (!unlink($filename)) {
                                    throw new \Exception('Unable to tidy up post-processed file: ' . $filename);
                                }
                            }
                        } else {
                            throw new \Exception('Unable to move file into storage directory /var/www/html/uploads - does it exist?');
                        }
                    } catch (\Exception $e) {
                        Alert::create('danger', 'Error: ' . $e->getMessage());
                    }
                    break;

                case 'update':
                    try {
                        $uploaddir = ROOT . '/uploads/';
                        $filename  = $uploaddir . time() . '_' . Slug::generate(basename($_FILES['upload']['name'])) . '.' . pathinfo($_FILES['upload']['name'], PATHINFO_EXTENSION);

                        if (move_uploaded_file($_FILES['upload']['tmp_name'], $filename)) {
                            $this->viewData['categories']  = R::find('categories', ' enabled = 1');
                            $this->viewData['oldFilename'] = basename($_FILES['upload']['name']);
                            $this->viewData['csvArray']    = array_map('str_getcsv', file($filename, FILE_IGNORE_NEW_LINES));

                            // removing the first line for field titles
                            $fieldTitles = array_shift($csvArray);

                            foreach ($csvArray as $upComponent) {
                                // <?=htmlentities($preComponent[3])
                                $component = R::findOne('components', ' itemcode = ?', [ htmlentities($upComponent[3]) ]);
                                if ($component != null) {
                                    $component->price = htmlentities($upComponent[5]);
                                    R::store($component);

                                    $updatedComponents .= '<li>(ID: ' . $component->id . ') ' . $component->name . '</li>';
                                }
                            }

                            Alert::create('success', 'Pricing update successfully uploaded: ' . basename($_FILES['upload']['name'])) . '. Updated components: <ul>' . $updatedComponents . '<\ul>';

                            if (!unlink($filename)) {
                                throw new \Exception('Unable to tidy up post-processed file: ' . $filename);
                            }
                        } else {
                            throw new \Exception('Unable to move file into storage directory /var/www/html/uploads - does it exist?');
                        }
                    } catch (\Exception $e) {
                        Alert::create('danger', 'Error: ' . $e->getMessage());
                    }
                    break;
            }
        }

        $this->viewOpts['page']['title']   = 'DC Admin | Import Files';

        $this->viewOpts['menu']['enabled'] = true;
        $this->viewOpts['menu']['content'] = 'admin';
        $this->viewOpts['menu']['section'] = 'components';

        $this->viewOpts['sidebar']['enabled'] = true;
        $this->viewOpts['sidebar']['content'] = 'components';
        $this->viewOpts['sidebar']['section'] = 'import';

        $this->viewOpts['footer']['enabled'] = false;
        $this->viewData['newComponents'] = R::find('components', ' enabled = 1 ORDER BY created DESC');

        $this->view->load($this->viewOpts, $this->viewData);
    }

    public function replace()
    {
        $this->viewOpts['page']['title']   = 'DC Admin | EOL Replacement';
        $this->viewOpts['page']['content'] = 'components/replace';

        $this->viewOpts['menu']['enabled'] = true;
        $this->viewOpts['menu']['content'] = 'admin';
        $this->viewOpts['menu']['section'] = 'components';

        $this->viewOpts['sidebar']['enabled'] = true;
        $this->viewOpts['sidebar']['content'] = 'components';
        $this->viewOpts['sidebar']['section'] = 'replace';

        $this->viewOpts['footer']['enabled'] = false;
        $this->viewData['newComponents'] = R::find('components', ' enabled = 1 ORDER BY created DESC');

        $this->view->load($this->viewOpts, $this->viewData);
    }
}
