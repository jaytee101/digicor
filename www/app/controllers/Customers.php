<?php

namespace app\Controllers;

use \app\Core\Controller as Controller;
use \app\Core\Log as Log;
use \app\Helpers\Alert as Alert;
use \app\Models\Auth as Auth;
use \app\Models\Customer as Customer;
use \app\Models\User as User;

use \R as R;

class Customers extends Controller
{
    public function admin()
    {
        if (empty($_SESSION['authenticated'])) {
            if ($_SESSION['user']['role'] != 'staff') {
                header("Location:/");
            }
        }

        $this->viewOpts['page']['title']   = 'DC Admin';
        $this->viewOpts['page']['content'] = 'customers/admin';

        $this->viewOpts['menu']['enabled'] = true;
        $this->viewOpts['menu']['content'] = 'admin';
        $this->viewOpts['menu']['section'] = 'customers';

        $this->viewOpts['sidebar']['enabled'] = true;
        $this->viewOpts['sidebar']['content'] = 'customers';
        $this->viewOpts['sidebar']['section'] = 'home';

        $this->viewOpts['footer']['enabled'] = false;

        if (empty($_SESSION['authenticated'])) {
            $this->viewOpts['menu']['enabled'] = false;
            $this->viewOpts['page']['content'] = 'admin/login';
        }

        $this->viewData['customers'] = R::find('users', ' role = ? AND enabled = 1', [ 'customer' ]);

        $this->view->load($this->viewOpts, $this->viewData);
    }
}
