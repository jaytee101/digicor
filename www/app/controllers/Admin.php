<?php

namespace app\Controllers;

use \app\Core\Controller as Controller;
use \app\Core\Log as Log;
use \app\Helpers\Alert as Alert;
use \app\Helpers\File as File;
use \app\Helpers\Time as Time;
use \app\Helpers\Slug as Slug;

use \app\Models\Auth as Auth;
use \app\Models\Import as Import;
use \app\Models\Component as Component;

use \R as R;

class Admin extends Controller
{
    public function index()
    {
        if (empty($_SESSION['authenticated'])) {
            header("Location:/");
        }

        $this->viewOpts['page']['title']   = 'DC Admin';
        $this->viewOpts['page']['content'] = 'admin/index';

        $this->viewOpts['menu']['enabled'] = true;
        $this->viewOpts['menu']['content'] = 'admin';
        $this->viewOpts['menu']['section'] = 'home';

        $this->viewOpts['footer']['enabled'] = false;

        if (empty($_SESSION['authenticated'])) {
            $this->viewOpts['menu']['enabled'] = false;
            $this->viewOpts['page']['content'] = 'admin/login';
        }

        $this->view->load($this->viewOpts, $this->viewData);
    }
}
