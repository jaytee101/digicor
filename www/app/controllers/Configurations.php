<?php
namespace app\Controllers;

use \app\Core\Controller as Controller;
use \app\Core\Log as Log;
use \app\Helpers\Alert as Alert;
use \app\Helpers\File as File;
use \app\Helpers\Time as Time;

use \app\Models\Category as Category;
use \app\Models\Component as Component;
use \app\Models\Configuration as Configuration;

use \R as R;

class Configurations extends Controller
{
    public function admin($disabled = false)
    {
        if (empty($_SESSION['authenticated'])) {
            header("Location:/");
        }

        if (!empty($_POST['action'])) {
            switch ($_POST['action']) {
                case 'create':
                    $create = Configuration::create();
                    if ($create == true) {
                        Alert::create('success', $_POST['name'] . ' has been created!');
                    }
                    break;

                case 'update':
                    Configuration::update($_POST['configuration']);
                    break;

                case 'delete':
                    Configuration::delete($_POST['configuration']);
                    break;

                case 'disable':
                    Configuration::disable($_POST['configuration']);
                    break;

                case 'enable':
                    Configuration::enable($_POST['configuration']);
                    break;

                case 'publish':
                    Configuration::publish($_POST['configuration']);
                    break;

                case 'unpublish':
                    Configuration::unpublish($_POST['configuration']);
                    break;
            }
        }

        $this->viewOpts['page']['title']   = 'DC Admin | Configurations';
        $this->viewOpts['page']['content'] = 'configurations/admin';

        $this->viewOpts['menu']['enabled'] = true;
        $this->viewOpts['menu']['content'] = 'admin';
        $this->viewOpts['menu']['section'] = 'configurations';

        $this->viewOpts['sidebar']['enabled'] = true;
        $this->viewOpts['sidebar']['content'] = 'configurations';
        $this->viewOpts['sidebar']['section'] = 'home';

        $this->viewOpts['footer']['enabled'] = false;

        $this->viewData['configurations'] = R::find('configurations', ' enabled = 1');

        if ($disabled == true) {
            $this->viewOpts['page']['content'] = 'configurations/disabled';
            $this->viewData['configurations'] = R::find('configurations', ' enabled = 0');
            $this->viewOpts['sidebar']['section'] = 'disabled';
        }

        $this->view->load($this->viewOpts, $this->viewData);
    }

    public function create()
    {
        if (empty($_SESSION['authenticated'])) {
            header("Location:/");
        }

        $this->viewOpts['page']['title']   = 'DC Admin | Configurations';
        $this->viewOpts['page']['content'] = 'configurations/create';

        $this->viewOpts['menu']['enabled'] = true;
        $this->viewOpts['menu']['content'] = 'admin';
        $this->viewOpts['menu']['section'] = 'configurations';

        $this->viewOpts['sidebar']['enabled'] = true;
        $this->viewOpts['sidebar']['content'] = 'configurations';
        $this->viewOpts['sidebar']['section'] = 'create';

        $this->viewOpts['footer']['enabled'] = false;

        $this->viewData['components'] = R::find('components', ' enabled = 1 ORDER BY name ASC');
        $this->viewData['categories'] = R::find('categories', ' visible = 1 AND enabled = 1 ORDER BY name ASC');

        $this->view->load($this->viewOpts, $this->viewData);
    }

    public function manage($id = 0)
    {
        if (empty($_SESSION['authenticated'])) {
            header("Location:/");
        }

        if (empty($id)) {
            Alert::create('danger', 'No config id provided.');
            header("Location:/admin/configurations");
        }

        $config = R::load('configurations', $id);

        if (empty($config)) {
            Alert::create('danger', 'No config data available.');
            header("Location:/admin/configurations");
        }

        if (!empty($_POST)) {
            switch ($_POST['action']) {
                case 'update':
                    Configuration::update($_POST['configuration']);
                    break;
            }
        }

        $this->viewOpts['page']['title']   = 'DC Admin | Configurations';
        $this->viewOpts['page']['content'] = 'configurations/manage';
        $this->viewOpts['page']['section'] = 'home';

        $this->viewOpts['menu']['enabled'] = true;
        $this->viewOpts['menu']['content'] = 'admin';
        $this->viewOpts['menu']['section'] = 'configurations';

        $this->viewOpts['sidebar']['enabled'] = true;
        $this->viewOpts['sidebar']['content'] = 'configurations';
        $this->viewOpts['sidebar']['section'] = 'manage';

        $this->viewOpts['footer']['enabled'] = false;

        $this->viewData['config'] = $config;
        $this->viewData['logs'] = R::find('config_logs', ' config = ?', [ $config->id ]);

        $this->view->load($this->viewOpts, $this->viewData);
    }

    public function manageSections($id = 0)
    {
        try {
            $config = R::load('configurations', $id);

            if ($config->id == 0) {
                throw new \Exception ('No configuration found (ID: ' . $id . ')');
            }
        } catch (\Exception $e) {
            Alert::create('danger', 'No configuration found');
            Log::error('Error: ' . $e->getMessage(), $e->getLine(), $e->getFile());
        }

        if (!empty($_POST)) {
            switch ($_POST['action']) {
                case 'ajax':
                    switch ($_POST['call']) {
                        case 'addNewSection':
                            $this->ajaxAddNewSection();
                            break;

                        case 'addNewGroup':
                            $this->ajaxAddNewGroup();
                            break;

                        case 'addComponent':
                            $this->ajaxAddComponent();
                            break;
                    }
                    exit();
                    break;

                case 'save':
                    $save = Configuration::createSection(); // confirmation message in Configuration Model
                    break;

                case 'delete':
                    $delete = Configuration::deleteSection();
                    break;

                case 'hideSection':
                    die('hide');
                    break;

                case 'showSection':
                    die('show');
                    break;
            }
        }

        $this->viewOpts['page']['title']   = 'DC Admin | Configurations';
        $this->viewOpts['page']['content'] = 'configurations/sections';
        $this->viewOpts['page']['section'] = 'sections';

        $this->viewOpts['menu']['enabled'] = true;
        $this->viewOpts['menu']['content'] = 'admin';
        $this->viewOpts['menu']['section'] = 'configurations';

        $this->viewOpts['sidebar']['enabled'] = true;
        $this->viewOpts['sidebar']['content'] = 'configurations';
        $this->viewOpts['sidebar']['section'] = 'manage';

        $this->viewOpts['footer']['enabled'] = false;

        $this->viewData['config']     = $config;
        $this->viewData['components'] = R::find('components', ' enabled = 1 ORDER BY name ASC');
        $this->viewData['categories'] = R::find('categories', ' visible = 1 AND enabled = 1 ORDER BY name ASC');
        $this->viewData['sections']   = R::find('config_sections', ' configuration = ? AND enabled = 1 ORDER BY id ASC', [ $config->id ]);

        $this->view->load($this->viewOpts, $this->viewData);
    }

    public function editSection($config = 0, $section = 0)
    {
        try {
            $config = R::load('configurations', $config);

            if ($config->id == 0) {
                throw new \Exception ('No configuration found (ID: ' . $config . ')');
            }

            $section = R::load('config_sections', $section);

            if ($section->id == 0) {
                throw new \Exception ('No section found (ID: ' . $section . ')');
            }
        } catch (\Exception $e) {
            Alert::create('danger', 'No configuration found');
            Log::error('Error: ' . $e->getMessage(), $e->getLine(), $e->getFile());
        }

        if (!empty($_POST)) {
            switch ($_POST['action']) {
                case 'ajax':
                    switch ($_POST['call']) {
                        case 'addNewSection':
                            $this->ajaxAddNewSection();
                            break;

                        case 'addNewGroup':
                            $this->ajaxAddNewGroup();
                            break;

                        case 'addComponent':
                            $this->ajaxAddComponent();
                            break;
                    }
                    exit();
                    break;

                case 'edit':
                    Component::update($_POST['component']);
                    break;

                case 'save':
                    $save = Configuration::updateSection($_POST['section']);

                    if ($save == true) {
                        Alert::create('success', 'Section: (' . $section->id . ') has been successfully updated!');
                        Log::activity('Config Section (' . $section->id . ') has been updated by ' . $_SESSION['user']['name'], __FILE__);
                    }
                    break;

                case 'delete':
                    $delete = Configuration::deleteSection();
                    break;
            }
        }

        $this->viewOpts['page']['title']   = 'DC Admin | Configurations';
        $this->viewOpts['page']['content'] = 'configurations/sectionEdit';
        $this->viewOpts['page']['section'] = 'sections';

        $this->viewOpts['menu']['enabled'] = true;
        $this->viewOpts['menu']['content'] = 'admin';
        $this->viewOpts['menu']['section'] = 'configurations';

        $this->viewOpts['sidebar']['enabled'] = true;
        $this->viewOpts['sidebar']['content'] = 'configurations';
        $this->viewOpts['sidebar']['section'] = 'manage';

        $this->viewOpts['footer']['enabled'] = false;

        $this->viewData['config']     = $config;
        $this->viewData['section']    = $section;

        $this->viewData['components'] = R::find('components', ' enabled = 1 ORDER BY name ASC');
        $this->viewData['categories'] = R::find('categories', ' visible = 1 AND enabled = 1 ORDER BY name ASC');
        $this->viewData['groups'] = R::find('config_groups', ' configuration = ? AND section = ? AND enabled = 1 ORDER BY ordering ASC', [ $config->id, $section->id ]);

        $this->view->load($this->viewOpts, $this->viewData);
    }

    //
    // AJAX FUNCTIONS BELOW
    //
    public function ajaxAddNewSection()
    {
        $sectionId = $_POST['section'];
        $groupId   = $_POST['group'];
        $editing = (!empty($_POST['editing'])) ? true : false;

        if ($editing == true) {
            $section = R::load('config_sections', $sectionId);
            $groups  = R::find('config_groups', ' configuration = ? AND section = ? AND enabled = 1 ORDER BY ordering ASC', [ $section->configuration, $sectionId ]);
        }
        ?>
        <div class="row" id="section-<?=$sectionId?>">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="form-row">
                            <div class="col-md-1">
                                <h4 class="text-warning">Section:</h4>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control sectionName-<?=$sectionId?>" name="sections[<?=$sectionId?>][name]" placeholder="Section Name" />
                            </div>
                            <div class="col-md-1">
                                <input type="text" class="form-control" name="sections[<?=$sectionId?>][itemlimit]" placeholder="Item Limit" />
                            </div>
                            <div class="col-md-3">
                                <div class="form-check">
                                    <input type="hidden" name="sections[<?=$sectionId?>][optional]" value="0">
                                    <input type="checkbox" name="sections[<?=$sectionId?>][optional]" id="optionalSection-<?=$sectionId?>" class="form-check-input" value="1">
                                    <label for="optionalSection-<?=$sectionId?>" class="form-check-label">Optional Section, customer can select nothing</label>
                                </div>
                                <div class="form-check">
                                    <input type="hidden" name="sections[<?=$sectionId?>][hidden]" value="0">
                                    <input type="checkbox" name="sections[<?=$sectionId?>][hidden]" id="hiddenSection-<?=$sectionId?>" class="form-check-input" value="1">
                                    <label for="hiddenSection-<?=$sectionId?>" class="form-check-label">Hide, customers can't see this whole section</label>
                                </div>
                            </div>
                            <div class="col-md-1 text-right">
                                <button type="button" class="btn btn-danger removeSection" data-id="<?=$sectionId?>"><i class="fa fa-times-circle"></i> Remove</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <b>Section Description:</b> &nbsp; <small class="text-muted"><em>(You can leave this empty if necessary)</em></small>
                            <br />
                            <textarea name="sections[<?=$sectionId?>][description]?>" class="form-control sectionDescription"></textarea>
                        </div>

                        <br />
                        <br />

                        <div class="row">
                            <div class="col">
                                <div class="card">
                                    <h5 class="card-header">Product Groups in this Section</h5>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-auto">
                                                <button type="button" class="btn btn-primary addNewGroup" data-id="<?=$sectionId?>">Add Group to Section</button>
                                            </div>
                                            <div class="col">
                                                <?php
                                                if ($editing == true) {
                                                    echo $groups;
                                                    foreach ($groups as $group) {
                                                        $this->ajaxAddNewGroup();
                                                    }
                                                }
                                                ?>
                                                <div class="groups-<?=$sectionId?>"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
            </div>
        </div>
        <?php
    }

    public function ajaxAddNewGroup()
    {
        $section = $_POST['section'];
        $group   = $_POST['group'];
        $editing = (!empty($_POST['editing'])) ? true : false;
        ?>
        <div class="row" id="group-<?=$group?>">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <input type="text" class="form-control" name="<?=$editing == true ? 'groups' : 'sections[' . $section . '][groups]'?>[<?=$group?>][name]" placeholder="Product Group Name" />
                                <div class="form-check">
                                    <input type="hidden" name="<?=$editing == true ? 'groups' : 'sections[' . $section . '][groups]'?>[<?=$group?>][hidden]" value="0">
                                    <input type="checkbox" name="<?=$editing == true ? 'groups' : 'sections[' . $section . '][groups]'?>[<?=$group?>][hidden]" id="hiddenGroup-<?=$section?>" class="form-check-input" value="1">
                                    <label for="hiddenGroup-<?=$section?>" class="form-check-label">Hide Group from Customers</label>
                                </div>
                                <small class="text-muted">If a group is hidden, all products within the group are automatically selected as part of the Configuration</small>
                                <br />
                                <br />
                                <button type="button" class="btn btn-block btn-primary addComponent" data-group="<?=$group?>" data-section="<?=$section?>"><i class="fa fa-plus-square"></i> Add Component to Group</button><br />
                                <button type="button" class="btn btn-block btn-danger removeGroup" data-group="<?=$group?>" data-section="<?=$section?>"><i class="fa fa-times-circle"></i> Remove Group</button>
                            </div>
                            <div class="col-md-9">
                                Optional Group Note to Customer:
                                <textarea name="<?=$editing == true ? 'groups' : 'sections[' . $section . '][groups]'?>[<?=$group?>][description]" class="form-control groupDescription"></textarea>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col">
                                <small class="text-muted">If marked as a Base Component, that component will be automatically selected when the Customer views the configuration. It will <b>also</b> be used to auto-calculate the base price</small>
                                <div class="components-<?=$group?>"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <?php
    }

    public function ajaxAddComponent()
    {
        $section   = $_POST['section'];
        $group     = $_POST['group'];
        $compCount = $_POST['component'];
        $editing   = (!empty($_POST['editing'])) ? true : false;

        $categories = R::find('categories', ' ORDER BY name ASC');
        ?>
            <div class="form-row row" id="component-<?=$compCount?>">
                <div class="col-md-1">
                    <div class="form-check">
                        <input type="hidden" name="<?=$editing == true ? 'groups' : 'sections[' . $section . '][groups]'?>[<?=$group?>][components][<?=$compCount?>][base]" value="0">
                        <input type="checkbox" name="<?=$editing == true ? 'groups' : 'sections[' . $section . '][groups]'?>[<?=$group?>][components][<?=$compCount?>][base]" id="baseComponent-<?=$section?>" class="form-check-input" value="1">
                        <label for="baseComponent-<?=$section?>" class="form-check-label">Base</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <select name="<?=$editing == true ? 'groups' : 'sections[' . $section . '][groups]'?>[<?=$group?>][components][<?=$compCount?>][id]" class="form-control">
                        <?php
                        $components = R::find('components', ' enabled = 1 ORDER BY name ASC');
                        foreach ($components as $component) {
                            ?>
                            <option value="<?=$component->id?>"><?=$component->name?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-1">
                    <input type="text" class="form-control" name="<?=$editing == true ? 'groups' : 'sections[' . $section . '][groups]'?>[<?=$group?>][components][<?=$compCount?>][step]" placeholder="Step Qty">
                </div>
                <div class="col-md-2">
                    <input type="text" class="form-control" name="<?=$editing == true ? 'groups' : 'sections[' . $section . '][groups]'?>[<?=$group?>][components][<?=$compCount?>][price]" placeholder="Price Override">
                </div>
                <div class="col-md-1">
                    <div class="form-check">
                        <input type="hidden" name="<?=$editing == true ? 'groups' : 'sections[' . $section . '][groups]'?>[<?=$group?>][hidden]" value="0">
                        <input type="checkbox" name="<?=$editing == true ? 'groups' : 'sections[' . $section . '][groups]'?>[<?=$group?>][hidden]" id="hiddenComponent-<?=$section?>" class="form-check-input" value="1">
                        <label for="hiddenComponent-<?=$section?>" class="form-check-label">Hide</label>
                    </div>
                </div>
                <div class="col-md-1">
                    <button type="button" class="btn btn-block btn-danger removeComponent" data-id="<?=$compCount?>"><i class="fa fa-trash-alt"></i></button>
                </div>
            </div>
        <?php
    }
}
