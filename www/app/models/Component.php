<?php

namespace app\Models;

use \app\Core\Debug as Debug;
use \app\Core\Model as Model;
use \app\Helpers\Alert as Alert;
use \app\Helpers\Slug as Slug;
use \app\Core\Log as Log;

use \app\Models\Category as Category;

use \R as R;

class Component extends Model
{
    public static function create($data = [])
    {
        try {
            if (empty($data)) {
                throw new \Exception('No component data provided');
            }

            if (!empty($_FILES['image']['name'])) {
                $uploaddir = ROOT . '/uploads/';
                $extension = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
                $filename = 'images/' . date('Y-m-d-H-i-s') . '_img_' . uniqid() . '.' . $extension;

                if (!move_uploaded_file($_FILES['image']['tmp_name'], $uploaddir . $filename)) {
                    throw new \Exception('could not move file to uploads folder');
                } else {
                    $data['picture'] = '/uploads/' . $filename;
                }
            }

            // detection work on the category submitted
            $catCheck = R::findOne('categories', ' `name` = ?', [ $data['category'] ]);
            if ($catCheck == null) {
                $newCat = R::xdispense('categories');
                $newCat['name']        = $data['category'];
                $newCat['slug']        = Slug::generate($data['category']);
                $newCat['description'] = null;
                $newCat['visible']     = true;
                $newCat['enabled']     = true;
                $catId = R::store($newCat);

                Log::activity('New Category Added: ' . $catId . ' - ' . $data['category'], __FILE__);
            } else {
                $catId = $catCheck->id;
            }

            // checking if a component already exists (based on SKU) - this is primarily called when file importing
            $component = R::findOne('components', ' itemcode = ?', [ $data['itemcode'] ]);

            if ($component != null) {
                // update the product
                $component['categoryId']  = $catId;
                $component['created']     = time();
                $component['updated']     = time();
                $component['itemcode']    = $data['itemcode'];
                $component['name']        = $data['name'];
                $component['description'] = $data['description'];
                $component['price']       = empty($data['price']) ? 0 : $data['price'] * 100;   // price is stored in cents!
                $component['image']       = ($data['picture'] == '/uploads/') ? null : (!empty($data['picture'])) ? $data['picture'] : null;
                $component['enabled']     = true;
                R::store($component);

                Log::activity('Component updated: ' . $data['itemcode'] . ' - ' . $data['name'], __FILE__);
            } else {
                $component = R::xdispense('components');
                $component['categoryId']  = $catId;
                $component['created']     = time();
                $component['updated']     = time();
                $component['itemcode']    = $data['itemcode'];
                $component['name']        = $data['name'];
                $component['description'] = empty($data['description']) ? null : $data['description'];
                $component['price']       = empty($data['price']) ? 0 : $data['price'] * 100;   // price is stored in cents!
                $component['image']       = ($data['picture'] == '/uploads/') ? null : (!empty($data['picture'])) ? $data['picture'] : null;
                $component['enabled']     = true;
                $componentId = R::store($component);
                Log::activity('New Component: ' . $data['itemcode'] . ' - ' . $data['name'], __FILE__);
            }
        } catch (\Exception $e) {
            Alert::create('danger', 'Error: (' . $e->getLine() . ') ' . $e->getMessage());
        }
    }

    /**
     * update a component
     * specifically handles manually editing a component, mass updates are done via file import
     * in the create() function
     *
     * @param integer $id
     * @return void
     */
    public static function update($id = 0)
    {
        try {
            if (empty($id)) {
                throw new \Exception('No component ID provided');
            }

            $component = R::load('components', $id);

            if ($component->id == 0) {
                throw new \Exception('No component found with ID: ' . $id);
            }

            if (!empty($_FILES['image']['name'])) {
                $uploaddir = ROOT . '/uploads/';
                $extension = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
                $filename = 'images/' . date('Y-m-d-H-i-s') . '_img_' . uniqid() . '.' . $extension;

                if (!move_uploaded_file($_FILES['image']['tmp_name'], $uploaddir . $filename)) {
                    throw new \Exception('could not move file to uploads folder');
                } else {
                    $component['image'] = '/uploads/' . $filename;
                }
            }

            // check the category we're trying to change (if necessary)
            $catCheck = R::findOne('categories', ' name = ?', [ $_POST['category'] ]);
            if ($catCheck == null) {
                $newCat = R::xdispense('categories');
                $newCat['name']        = $_POST['category'];
                $newCat['slug']        = Slug::generate($_POST['category']);
                $newCat['description'] = null;
                $newCat['visible']     = true;
                $newCat['enabled']     = true;
                $catId = R::store($newCat);
            } else {
                $catId = $catCheck->id;
            }

            // now we update component bits
            $component['categoryId']  = $catId;
            $component['updated']     = time();
            $component['itemcode']    = $_POST['itemcode'];
            $component['name']        = $_POST['name'];
            $component['description'] = $_POST['description'];
            $component['price']       = empty($_POST['price']) ? 0 : $_POST['price'];

            // if remove image is checked
            if ($_POST['removeImg'] == true) {
                $component['image'] = null;
            }

            $componentId = R::store($component);

            Alert::create('success', 'Component: ' . $_POST['name'] . ' has been successfully updated');
        } catch (\Exception $e) {
            Alert::create('danger', 'Error: (' . $e->getLine() . ') ' . $e->getMessage());
        }
    }

    public static function disable($id = 0)
    {
        try {
            if (empty($id)) {
                throw new \Exception('No component ID provided');
            }

            $component = R::load('components', $id);

            if ($component->id == 0) {
                throw new \Exception('No component found with ID: ' . $id);
            }

            $componentName = $component->name;
            $component->enabled = false;
            $componentId = R::store($component);

            Alert::create('danger', 'Component: ' . $componentName . ' has been successfully disabled');
        } catch (\Exception $e) {
            Alert::create('danger', 'Error: (' . $e->getLine() . ') ' . $e->getMessage());
        }
    }

    public static function enable($id = 0)
    {
        try {
            if (empty($id)) {
                throw new \Exception('No component ID provided');
            }

            $component = R::load('components', $id);

            if ($component->id == 0) {
                throw new \Exception('No component found with ID: ' . $id);
            }

            $componentName = $component->name;
            $component->enabled = true;
            $componentId = R::store($component);

            Alert::create('success', 'Component: ' . $componentName . ' has been successfully re-enabled');
        } catch (\Exception $e) {
            Alert::create('danger', 'Error: (' . $e->getLine() . ') ' . $e->getMessage());
        }
    }

    public static function get($id = 0, $field = null)
    {
        try {
            if (empty($id)) {
                throw new \Exception('No component ID provided');
            }

            if (empty($field)) {
                throw new \Exception('No component field provided');
            }

            $component = R::load('components', $id);

            if ($component->id == 0) {
                throw new \Exception('No component found with ID: ' . $id);
            }

            return $component->$field;
        } catch (\Exception $e) {
            Log::actvity('Error: (' . $e->getLine() . ') ' . $e->getMessage(), __FILE__);
        }
    }
}
