<?php

namespace app\Models;

use \app\Core\Debug as Debug;
use \app\Core\Model as Model;
use \app\Helpers\Alert as Alert;
use \app\Core\Log as Log;
use \R as R;

use \app\Models\Component;
use \app\Models\Category;

class Configuration extends Model
{
    /**
     * log an action relevant to a specific config
     *
     * @param string $message
     * @param integer $id
     * @return void
     */
    public static function log($message = '', $id = 0)
    {
        try {
            if (empty($id)) {
                throw new \Exception('No configuration ID specified');
            }

            if (empty($message)) {
                throw new \Exception('No log message content specified');
            }

            $log = R::xdispense('config_logs');
            $log['time'] = time();
            $log['config'] = $id;
            $log['user'] = $_SESSION['user']['uuid'];
            $log['message'] = $message;
            R::store($log);
        } catch (\RedBeanPHP\RedException\SQL $e) {
            Log::error('Config Logging DB Error: ' . $id, $e->getLine(), $e->getFile());
            Alert::create('danger', 'Config Logging DB Error: (' . $e->getLine() . ') ' . $e->getMessage());
        } catch (\Exception $e) {
            Log::error('Error logging configuration activity: ' . $id, $e->getLine(), $e->getFile());
            Alert::create('danger', 'Config Logging Error: (' . $e->getLine() . ') ' . $e->getMessage());
        }
    }

    /**
     * create
     * create a config and enter it into the DB
     *
     * @return bool
     */
    public static function create()
    {
        try {
            if (empty($_POST)) {
                throw new \Exception('No data provided for Config creation');
            }

            $config = R::xdispense('configurations');
            $config['created']     = time();
            $config['updated']     = null;
            $config['sku']         = empty($_POST['sku']) ? null : $_POST['sku'];
            $config['name']        = $_POST['name'];
            $config['description'] = empty($_POST['description']) ? null: $_POST['description'];
            $config['price']       = empty($_POST['price']) ? 0 : $_POST['price'];
            $config['autoprice']   = $_POST['autoprice'];
            $config['published']   = null;
            $config['enabled']     = true;
            $configId = R::store($config);

            // set a name for the config if it's empty
            if (empty($_POST['name'])) {
                $config = R::load('configurations', $configId);
                $config['name'] = 'Config ' . $configId;
                R::store($config);
            }

            self::log($_POST['name'] . ' (ID: ' . $configId . ') has been created, successfully', $configId);
            Log::activity('New configuration: ' . $_POST['name'] . ' has just been created', __FILE__);

            return true;
        } catch (\Exception $e) {
            Alert::create('danger', 'Error: (' . $e->getLine() . ') ' . $e->getMessage());
            Log::error($e->getMessage(), $e->getLine(), $e->getFile());
        }

        return false;
    }

    /**
     * delete
     * delete a config PERMANENTLY FROM THE DATABASE
     * this is NOT undo-able
     *
     * @param integer $id
     * @return void
     */
    public static function delete($id = 0)
    {
        try {
            if (empty($_POST)) {
                throw new \Exception('No data provided for Config Deletion');
            }

            if (empty($id)) {
                throw new \Exception('No ID provided for Configuration');
            }

            $config = R::load('configurations', $id);
            $name = $config->name;

            if ($config->id == 0) {
                throw new \Exception('No configuration found!');
            }

            R::trash($config);

            Alert::create('danger', '(ID: ' . $id . ') ' . $name . ' has been deleted');
            Log::activity('Configuration: ' . $name . ' (id: ' . $id . ') has just been deleted', __FILE__);
        } catch (\Exception $e) {
            Log::error('Error Deletion configuration with ID: ' . $id, $e->getLine(), $e->getFile());

            Alert::create('danger', 'Config Deletion Error: (' . $e->getLine() . ') ' . $e->getMessage());
        }
    }

    /**
     * disable
     * disable a config in the DB - prevents customers from selecting and using the config
     *
     * @param integer $id
     * @return void
     */
    public static function disable($id = 0)
    {
        try {
            if (empty($_POST)) {
                throw new \Exception('No data provided for Config disabling');
            }

            if (empty($id)) {
                throw new \Exception('No ID provided for Configuration');
            }

            $config = R::load('configurations', $id);

            if ($config->id == 0) {
                throw new \Exception('No configuration found!');
            }

            $config->enabled = false;
            R::store($config);

            self::log('Config disabled by ' . $_SESSION['user']['name'], $config->id);
            Log::activity('Configuration: ' . $config->name . ' (id: ' . $config->id . ') has just been disabled', __FILE__);

            Alert::create('danger', '(ID: ' . $config->id . ') ' . $config->name . ' has been disabled');
        } catch (\Exception $e) {
            Log::error('Error disabling configuration with ID: ' . $id, $e->getLine(), $e->getFile());

            Alert::create('danger', 'Config Disabling Error: (' . $e->getLine() . ') ' . $e->getMessage());
        }
    }

    /**
     * enable
     * enable a config in the DB
     *
     * @param integer $id
     * @return void
     */
    public static function enable($id = 0)
    {
        try {
            if (empty($_POST)) {
                throw new \Exception('No data provided for Config enabling');
            }

            if (empty($id)) {
                throw new \Exception('No ID provided for Configuration');
            }

            $config = R::load('configurations', $id);

            if ($config->id == 0) {
                throw new \Exception('No configuration found!');
            }

            $config->enabled = true;
            R::store($config);

            self::log('Config enabled by ' . $_SESSION['user']['name'], $config->id);
            Log::activity('Configuration: ' . $config->name . ' (id: ' . $config->id . ') has just been enabled', __FILE__);

            Alert::create('success', $config->name . ' has been enabled');
        } catch (\Exception $e) {
            Log::error('Error enabling configuration with ID: ' . $id, $e->getLine(), $e->getFile());

            Alert::create('danger', 'Config enabling Error: (' . $e->getLine() . ') ' . $e->getMessage());
        }
    }

    public static function publish($id = 0)
    {
        try {
            if (empty($_POST)) {
                throw new \Exception('No data provided for Config publishing');
            }

            if (empty($id)) {
                throw new \Exception('No ID provided for Configuration');
            }

            $config = R::load('configurations', $id);

            if ($config->id == 0) {
                throw new \Exception('No configuration found!');
            }

            $config->published = time();
            R::store($config);

            self::log('Config published by ' . $_SESSION['user']['name'], $config->id);
            Log::activity('Configuration: ' . $config->name . ' (id: ' . $config->id . ') has just been published', __FILE__);

            Alert::create('success', $config->name . ' has been published');
        } catch (\Exception $e) {
            Log::error('Error publishing configuration with ID: ' . $id, $e->getLine(), $e->getFile());

            Alert::create('danger', 'Config Publishing Error: (' . $e->getLine() . ') ' . $e->getMessage());
        }
    }

    public static function unpublish($id = 0)
    {
        try {
            if (empty($_POST)) {
                throw new \Exception('No data provided for Config unpublishing');
            }

            if (empty($id)) {
                throw new \Exception('No ID provided for Configuration');
            }

            $config = R::load('configurations', $id);

            if ($config->id == 0) {
                throw new \Exception('No configuration found!');
            }

            $config->published = null;
            R::store($config);

            self::log('Config unpublished by ' . $_SESSION['user']['name'], $config->id);
            Log::activity('Configuration: ' . $config->name . ' (id: ' . $config->id . ') has just been unpublished', __FILE__);

            Alert::create('danger', $config->name . ' has been unpublished');
        } catch (\Exception $e) {
            Alert::create('danger', 'Config Unpublishing Error: (' . $e->getLine() . ') ' . $e->getMessage());
            Log::error($e->getMessage(), $e->getLine(), $e->getFile());
        }
    }

    public static function update($id = 0)
    {
        try {
            if (empty($_POST)) {
                throw new \Exception('No data provided for configuration update');
            }

            $config = R::load('configurations', $id);

            if ($config->id == 0) {
                throw new \Exception('Configuration not found');
            }

            // update the configuration
            $config['updated']     = time();
            $config['sku']         = empty($_POST['sku']) ? null : $_POST['sku'];
            $config['name']        = $_POST['name'];
            $config['description'] = empty($_POST['description']) ? null: $_POST['description'];
            $config['price']       = empty($_POST['price']) ? 0 : $_POST['price'];
            $config['autoprice']   = $_POST['autoprice'];
            R::store($config);

            Log::activity('Configuration: ' . $config->name . ' (id: ' . $config->id . ') has just been updated', __FILE__);
            self::log('Config details updated by ' . $_SESSION['user']['name'], $config->id);
        } catch (\Exception $e) {
            Alert::create('danger', 'Config Update Error: (' . $e->getLine() . ') ' . $e->getMessage());
            Log::error($e->getMessage(), $e->getLine(), $e->getFile());
        }
    }

    public static function createSection()
    {
        $sectionCount   = 0;
        $groupCount     = 0;
        $componentCount = 0;

        try {
            if (empty($_POST)) {
                throw new \Excepetion('No data found');
            }

            $config = R::load('configurations', $_POST['config']);

            if ($config->id == 0) {
                throw new \Exception('Configuration not found');
            }

            // if autoprice is checked - reset the price so it can be auto calculated
            if ($config->autoprice == 1) {
                $config->price = 0;
            }

            foreach ($_POST['sections'] as $sect) {
                $section = R::xdispense('config_sections');
                $section['configuration'] = $config->id;
                $section['created']       = time();
                $section['updated']       = null;
                $section['name']          = empty($sect['name']) ? null : $sect['name'];
                $section['description']   = empty($sect['description']) ? null: $sect['description'];
                $section['itemlimit']     = empty($sect['itemlimit']) ? 1 : $sect['itemlimit'];
                $section['optional']      = $sect['optional'];
                $section['ordering']      = 0;
                $section['hidden']        = false;
                $section['enabled']       = true;
                $sectionId = R::store($section);

                if ($sectionId != 0) {
                    self::log('New section added to config by ' . $_SESSION['user']['name'], $config->id);
                    $sectionCount++;
                }

                // parse the groups per section
                foreach ($sect['groups'] as $g) {
                    $group = R::xdispense('config_groups');
                    $group['configuration'] = $config->id;
                    $group['section']       = $sectionId;
                    $group['created']       = time();
                    $group['updated']       = null;
                    $group['name']          = $g['name'];
                    $group['description']   = empty($g['description']) ? null : $g['description'];
                    $group['ordering']      = 0;
                    $group['hidden']        = $g['hidden'];
                    $group['enabled']       = true;
                    $groupId = R::store($group);

                    if ($groupId != 0) {
                        self::log('New component group added to config by ' . $_SESSION['user']['name'], $config->id);
                        $groupCount++;
                    }

                    // parse the components per group per section
                    foreach ($g['components'] as $c) {
                        $componentDetails = R::load('components', $c['id']);

                        if ($componentDetails->id == 0) {
                            throw new \Exception('Component Details not found (ID: ' . $c['id'] . ')');
                        }
                        $comp = R::xdispense('config_components');
                        $comp['configuration'] = $config->id;
                        $comp['section']       = $sectionId;
                        $comp['group']         = $groupId;
                        $comp['created']       = time();
                        $comp['updated']       = null;
                        $comp['component']     = $c['id'];
                        $comp['base']          = $c['base'];
                        $comp['qty']           = empty($c['qty']) ? 0 : $c['qty'];
                        $comp['step']          = empty($c['step']) ? 1 : $c['step'];
                        $comp['price']         = empty($c['price']) ? $componentDetails->price : $c['price'];
                        $comp['ordering']      = 0;
                        $comp['note']          = $c['note'];
                        $comp['hidden']        = $c['hidden'];
                        $comp['enabled']       = true;
                        $compId = R::store($comp);

                        if ($compId != 0) {
                            self::log('New component added to config: [' . Component::get($c['id'], 'name') . '] by ' . $_SESSION['user']['name'], $config->id);
                            $componentCount++;
                        }

                        // auto-update pricing if the configuration has autoprice enabled
                        if (($config->autoprice == true) && ($comp['base'] == true)) {
                            $config->autoprice += $comp['price'] * $comp['qty'];
                        }
                    }
                }
            }

            Alert::create('success', 'Config updated with: <ul><li>' . $sectionCount . ' sections</li><li>' . $groupCount . ' component groups</li><li>' . $componentCount . ' components</li></ul>');
            Log::activity('Configuration: ' . $config->name . ' (id: ' . $config->id . ') has just been updated: <ul><li>' . $sectionCount . ' sections</li><li>' . $groupCount . ' component groups</li><li>' . $componentCount . ' components</li></ul>', __FILE__);

            return true;
        } catch (\Exception $e) {
            Alert::create('danger', 'Config Update Error: (' . $e->getLine() . ') ' . $e->getMessage());
            Log::error($e->getMessage(), $e->getLine(), $e->getFile());
        }

        return false;
    }

    public static function deleteSection()
    {
        try {
            if (empty($_POST)) {
                throw new \Exception("No data provided");
            }

            $section = R::load('config_sections', $_POST['section']);

            if ($section->id == 0) {
                throw new \Exception("No section found with ID: {$_POST['section']}");
            }

            $section['enabled'] = false;
            R::store($section);

            self::log('Section with ID: ' . $section->id . ' has been disabled by ' . $_SESSION['user']['name'], $section->configuration);
            Alert::create('success', 'Config (ID: ' . $section->configuration . ') has been updated - Section removed [ ' . $section->name . ' ]');
            Log::activity('Config (ID: ' . $section->configuration . ') has been updated - Section removed [ ' . $section->name . ' ]', __FILE__);

            return true;
        } catch (\Exception $e) {
            Alert::create('danger', 'Error: (' . $e->getLine() . ') ' . $e->getMessage());
            Log::error($e->getMessage(), $e->getLine(), $e->getFile());
        }

        return false;
    }

    public static function updateSection($id = 0)
    {
        try {
            if (empty($_POST)) {
                throw new \Exception("No data provided");
            }

            $section = R::load('config_sections', $_POST['section']);

            if ($section->id == 0) {
                throw new \Exception("No section found with ID: {$_POST['section']}");
            }

            $section['name'] = $_POST['name'];
            $section['itemlimit'] = $_POST['itemlimit'];
            $section['optional'] = $_POST['optional'];
            $section['hidden'] = $_POST['hidden'];
            $section['description'] = $_POST['description'];
            R::store($section);

            // remove all groups tied to the section
            $trashGroups = R::find('config_groups', ' configuration = ? AND section = ?', [ $section->configuration, $section->id ]);
            foreach ($trashGroups as $trashGroup) {
                R::trash($trashGroup);
            }

            // remove components tied to the group
            $trashComponents = R::find('config_components', ' configuration = ? AND section = ?', [ $section->configuration, $section->id ]);
            foreach ($trashComponents as $trashComp) {
                R::trash($trashComp);
            }

            foreach ($_POST['groups'] as $group) {
                // group we've added in editing
                $g = R::xdispense('config_groups');
                $g['configuration'] = $section->configuration;
                $g['section']       = $section->id;
                $g['created']       = time();
                $g['updated']       = null;
                $g['name']          = $group['name'];
                $g['description']   = empty($group['description']) ? null : $group['description'];
                $g['ordering']      = 0;
                $g['hidden']        = $group['hidden'];
                $g['enabled']       = true;
                $gId = R::store($g);

                // sorting the components that are entered
                foreach ($group['components'] as $component) {
                    // component adding
                    $c = R::xdispense('config_components');
                    $c['configuration'] = $section->configuration;
                    $c['section']       = $section->id;
                    $c['group']         = $gId;
                    $c['created']       = time();
                    $c['updated']       = null;
                    $c['component']     = $component['id'];
                    $c['base']          = $component['base'];
                    $c['step']          = empty($component['step']) ? 1 : $component['step'];
                    $c['price']         = empty($component['price']) ? 0 : $component['price'];
                    $c['ordering']      = 0;
                    $c['note']          = $component['note'];
                    $c['hidden']        = empty($component['hidden']) ? false : true;
                    $c['enabled']       = true;
                    R::store($c);
                }
            }

            self::log('Section updated by, ' . $_SESSION['user']['name'], $section->configuration);

            return true;
        } catch (\Exception $e) {
            Alert::create('danger', 'Error: (' . $e->getLine() . ') ' . $e->getMessage());
            Log::error($e->getMessage(), $e->getLine(), $e->getFile());
        }

        return false;
    }
}
