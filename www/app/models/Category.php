<?php

namespace app\Models;

use \app\Core\Debug as Debug;
use \app\Core\Model as Model;
use \app\Helpers\Alert as Alert;
use \app\Helpers\Slug as Slug;
use \app\Core\Log as Log;
use \R as R;

class Category extends Model
{
    public static function create()
    {
        try {
            if (empty($_POST['name'])) {
                throw new \Exception('Category Name is required');
            }

            $catNameCheck = R::findOne('categories', ' name = ?', [ $_POST['name'] ]);

            if (!empty($catNameCheck)) {
                throw new \Exception('Category Name is already in use, it must be unique');
            }

            $category = R::dispense('categories');
            $category['name']        = $_POST['name'];
            $category['slug']        = Slug::generate($_POST['name']);
            $category['description'] = $_POST['description'];
            $category['visible']     = true;
            $category['enabled']     = true;
            $categoryId = R::store($category);

            $catCheck = R::load('categories', $categoryId);

            if (!empty($catCheck)) {
                Alert::create('success', 'New Category <b>' . $_POST['name'] . '</b> has been created!');
                Log::activity('New Category: ' . $_POST['name'] . ' has been created', __FILE__);
            } else {
                throw new \Exception('Failed Category created check');
            }
        } catch (\Exception $e) {
            Alert::create('danger', 'Error: (' . $e->getLine() . ') ' . $e->getMessage());
        }
    }

    public static function show($id = 0)
    {
        try {
            if (empty($id)) {
                throw new \Exception('No category ID provided');
            }

            $category = R::load('categories', $id);
            $category['visible'] = true;
            R::store($category);

            $catCheck = R::load('categories', $id);

            if (!empty($catCheck)) {
                Alert::create('success', 'Category <b>' . $catCheck->name . '</b> is now visible to customers!');
                Log::activity('Category: ' . $catCheck->name . ' has been shown', __FILE__);
            } else {
                throw new \Exception('No category found');
            }
        } catch (\Exception $e) {
            Alert::create('danger', 'Error: (' . $e->getLine() . ') ' . $e->getMessage());
        }
    }

    public static function hide($id = 0)
    {
        try {
            if (empty($id)) {
                throw new \Exception('No category ID provided');
            }

            $category = R::load('categories', $id);
            $category['visible'] = false;
            R::store($category);

            $catCheck = R::load('categories', $id);

            if (!empty($catCheck)) {
                Alert::create('warning', 'Category <b>' . $catCheck->name . '</b> is now hidden from customers!');
                Log::activity('Category: ' . $catCheck->name . ' has been hidden', __FILE__);
            } else {
                throw new \Exception('No category found');
            }
        } catch (\Exception $e) {
            Alert::create('danger', 'Error: (' . $e->getLine() . ') ' . $e->getMessage());
        }
    }

    public static function disable($id = 0)
    {
        try {
            if (empty($id)) {
                throw new \Exception('No category ID provided');
            }

            $category = R::load('categories', $id);
            $category['enabled'] = false;
            R::store($category);

            $catCheck = R::load('categories', $id);

            if (!empty($catCheck)) {
                Alert::create('danger', 'Category <b>' . $catCheck->name . '</b> is now disabled from the back-end');
                Log::activity('Category: ' . $catCheck->name . ' has been disabled', __FILE__);
            } else {
                throw new \Exception('No category found');
            }
        } catch (\Exception $e) {
            Alert::create('danger', 'Error: (' . $e->getLine() . ') ' . $e->getMessage());
        }
    }

    public static function enable($id = 0)
    {
        try {
            if (empty($id)) {
                throw new \Exception('No category ID provided');
            }

            $category = R::load('categories', $id);
            $category['enabled'] = true;
            R::store($category);

            $catCheck = R::load('categories', $id);

            if (!empty($catCheck)) {
                Alert::create('success', 'Category <b>' . $catCheck->name . '</b> is now re-enabled in the back-end');
                Log::activity('Category: ' . $catCheck->name . ' has been enabled', __FILE__);
            } else {
                throw new \Exception('No category found');
            }
        } catch (\Exception $e) {
            Alert::create('danger', 'Error: (' . $e->getLine() . ') ' . $e->getMessage());
        }
    }

    public static function delete($id = 0)
    {
        try {
            if (empty($id)) {
                throw new \Exception('No category ID provided');
            }

            $category = R::load('categories', $id);
            $name = $category->name;
            R::trash($category);

            $catCheck = R::load('categories', $id);

            if ($catCheck->id == 0) {
                throw new \Exception('Category object not deleted from the DB');
            } else {
                Alert::create('danger', 'Category <b>' . $name . '</b> has been deleted');
                Log::activity('Category: ' . $name . ' has been deleted', __FILE__);
            }
        } catch (\Exception $e) {
            Alert::create('danger', 'Error: (' . $e->getLine() . ') ' . $e->getMessage());
        }
    }

    public static function get($id = 0, $field = null)
    {
        try {
            if (empty($id)) {
                throw new \Exception('No category ID provided');
            }

            $category = R::load('categories', $id);

            if ($category->id == 0) {
                throw new \Exception('No category found with id: ' . $id);
            }

            return $category->$field;
        } catch (\Exception $e) {
            Alert::create('danger', 'Error: (' . $e->getLine() . ') ' . $e->getMessage());
        }

        return false;
    }

    public static function update($id = 0)
    {
        try {
            if (empty($id)) {
                throw new \Exception('No category ID provided');
            }

            $category = R::load('categories', $id);

            if ($category->id == 0) {
                throw new \Exception('No category found with id: ' . $id);
            } else {
                $category['name']        = $_POST['name'];
                $category['slug']        = Slug::generate($_POST['name']);
                $category['description'] = $_POST['description'];
                R::store($category);

                Alert::create('success', 'Category: ' . $_POST['name'] . ' has been updated');
                Log::activity('Category: ' . $_POST['name'] . ' has been updated', __FILE__);
            }
        } catch (\Exception $e) {
            Alert::create('danger', 'Error: (' . $e->getLine() . ') ' . $e->getMessage());
        }
    }
}
