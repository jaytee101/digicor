<?php

namespace app\Models;

use \app\Core\Debug as Debug;
use \app\Core\Model as Model;
use \app\Helpers\Alert as Alert;
use \app\Core\Log as Log;
use \R as R;

class Auth extends Model
{
    public static function verify()
    {
        // ip address
        $address = $_SERVER['REMOTE_ADDR'];

        // user checking stuff
        $user = R::findOne('users', ' email = :login AND enabled = 1', [ 'login' => $_POST['login'] ]);

        // bad details check
        if ($user == null) {
            // no user found
            Log::access('Someone tried to log in with bad details from: ' . $address);
            Alert::create('danger', 'Sorry, no user found with those details!');

            return false;
        } else {
            // disabled user check
            if ($user->enabled != true) {
                Log::access('Someone tried to log in to a disabled account (' . $_POST['login'] . ') from: ' . $address);
                Alert::create('danger', "Sorry, you've tried to login with a disabled account!");

                return false;
            } else {
                // password verification
                if (password_verify(base64_encode($_POST['password']), $user['password'])) {
                    $_SESSION['authenticated']    = true;
                    $_SESSION['user']['uuid']     = $user->uuid;
                    $_SESSION['user']['name']     = $user->name;
                    $_SESSION['user']['email']    = $user->email;
                    $_SESSION['user']['role']     = $user->role;
                    $_SESSION['owner']            = ($user->email == 'johnathan.tiong@gmail.com') ? true : false;

                    $user->lastonline = time();
                    R::store($user);

                    // create a user session to remember that they're logged in
                    $session            = R::xdispense('users_sessions');
                    $session['time']    = time();
                    $session['user']    = $user->uuid;
                    $session['session'] = session_id();
                    $session['ip']      = $address;
                    $session = R::store($session);

                    // set a cookie to remember for 1 year
                    $expiry = time() + (86400 * 365 * 1);
                    $expiry = intval($expiry);

                    setcookie('digicor.configurator', session_id() . ':' . $session, $expiry, "/");

                    Log::access($user->name . ' has logged in successfully from: ' . $address);
                    Alert::create('success', "Hi $user->name! You're now logged in!");

                    return true;
                } else {
                    // bad password
                    Log::access($user->name . ' attempted to log in with a bad password from: ' . $address);
                    Alert::create('danger', "Login failure - bad password");

                    return false;
                }
            }
        }
    }
}
