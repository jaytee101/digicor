<?php

namespace app\Models;

use \app\Core\Debug as Debug;
use \app\Core\Model as Model;
use \app\Helpers\Alert as Alert;
use \app\Helpers\Uuid as Uuid;
use \app\Core\Log as Log;
use \R as R;

class User extends Model
{
    public static function create()
    {
        $time = time();

        try {
            /*

            google recaptcha code
            ---------------------
            @todo work with henry to register a digicor based google recaptcha account

            $response = $_POST['g-recaptcha-response'];
            $url = 'https://www.google.com/recaptcha/api/siteverify';
            $data = array(
                'secret'   => CAPTCHA_SECRET,
                'response' => $_POST["g-recaptcha-response"]
            );
            $options = array(
                'http' => array (
                    'method'  => 'POST',
                    'content' => http_build_query($data)
                )
            );
            $context         = stream_context_create($options);
            $verify          = file_get_contents($url, false, $context);
            $captcha_success = json_decode($verify);

            if ($captcha_success->success == false) {
                throw new \Exception("ReCaptcha verification failed");
            }

            */

            if (empty($_POST['email'])) {
                throw new \Exception('Email address is required!');
            }

            if ($_POST['password'] != $_POST['confirm']) {
                throw new \Exception('Password mismatch - you must match your password and confirm it too');
            }

            $user = R::xdispense('users');
            $user['created']    = $time;
            $user['updated']    = null;
            $user['uuid']       = Uuid::generate($_POST['email'], $time);
            $user['name']       = $_POST['firstname'] . ' ' . $_POST['lastname'];
            $user['email']      = $_POST['email'];
            $user['password']   = password_hash(base64_encode($_POST['password']), PASSWORD_BCRYPT);
            $user['company']    = $_POST['company'];
            $user['phone']      = $_POST['contact'];
            $user['role']       = 'customer';
            $user['lastonline'] = time();
            $user['enabled']    = true;
            R::store($user);

            return true;
        } catch (\RedBeanPHP\RedException\SQL $e) {
            Alert::create('danger', 'Error: ' . $e->getMessage());
            Log::error('User Create DB Error: ' . $e->getMessage(), $e->getFile(), $e->getLine());
        } catch (\Exception $e) {
            Log::error('User Creation Error: ' . $e->getMessage(), $e->getFile(), $e->getLine());
        }

        return false;
    }

    public static function getByUuid($uuid = '', $field = '')
    {
        try {
            if (empty($uuid)) {
                throw new \Exception('No user ID provided');
            }

            if (empty($field)) {
                throw new \Exception('No field specified');
            }

            $user = R::findOne('users', ' uuid = ?', [ $uuid ]);

            if ($user == null) {
                throw new \Exception('No user found!');
            }

            return $user->$field;
        } catch (\RedBeanPHP\RedException\SQL $e) {
            Alert::create('danger', 'Error: ' . $e->getMessage());
            Log::error('DB Error: ' . $e->getMessage(), $e->getFile(), $e->getLine());
        } catch (\Exception $e) {
            Log::error('Error: ' . $e->getMessage(), $e->getFile(), $e->getLine());
        }
    }
}
