        </div>

        <div id="foot">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <hr style="border-top: 1px dashed #444; ">
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <small class="text-muted">
                            Copyright &copy; Johnathan Tiong, 2018. All Rights Reserved.
                        </small>
                    </div>
                    <div class="col text-right">
                        <a href="/changelog" class="badge badge-secondary">
                            <?php
                            try {
                                if (file_exists(ROOT . '/version')) {
                                    $version = file_get_contents(ROOT . '/version');
                                    throw new \Exception($version);
                                } else {
                                    throw new \Exception('local.' . date('Y.m.d'));
                                }
                            } catch (\Exception $e) {
                                echo $e->getMessage();
                            }
                            ?>
                        </a>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col">
                        <small class="text-muted">User is located in <?=USER_COUNTRY?> (<?=$_SESSION['user']['location']['country']?>)</small>
                    </div>
                </div>
                <br />

            </div>
        </div>

    </body>
</html>
