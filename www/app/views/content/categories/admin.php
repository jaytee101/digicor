<div class="row">
    <div class="col">
        <div class="card">
            <h1 class="card-header">Categories</h1>
            <div class="card-body">
                <h5 class="card-title text-warning">Instructions</h5>
                <p class="card-text">
                    Create categories which components belong to with the button on the left (Create a new Category). You can then update
                    their Names, Descriptions, Visibility to Customers, and Enable/Disable them for staff use.<br />
                    <b class="text-warning">Category names must be unique</b> and you can only delete a category, <u>once it has been disabled</u>.
                </p>
            </div>
        </div>

        <br />
    </div>
</div>

<div class="row">
    <div class="col">
        <table class="table table-hover datatable">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Visible</th>
                    <th>Options</th>
                </tr>
            </thead>
            <tbody>
            <?php
            foreach ($categories as $category) {
                ?>
                <tr>
                    <td><?=$category->name?></td>
                    <td><?=$category->description?></td>
                    <td>
                        <form action="/admin/categories" method="post">
                            <input type="hidden" name="action" value="<?=$category->visible ? 'hide' : 'show'?>">
                            <input type="hidden" name="category" value="<?=$category->id?>">
                            <?php
                            if ($category->visible == true) {
                                ?>
                                <button class="btn btn-sm btn-success"><i class="fa fa-eye"></i> <small>(click to hide)</small></button>
                                <?php
                            } else {
                                ?>
                                <button class="btn btn-sm btn-secondary"><i class="fa fa-eye-slash"></i> <small>(click to show)</small></button>
                                <?php
                            }
                            ?>
                        </form>
                    </td>
                    <td class="d-flex justify-content-around">
                        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#editCategory-<?=$category->id?>">Edit</button>

                        <?php
                        if ($category->enabled == true) {
                            ?>
                            <form action="/admin/categories" method="post">
                                <input type="hidden" name="action" value="disable">
                                <input type="hidden" name="category" value="<?=$category->id?>">
                                <button class="btn btn-sm btn-success">Enabled (click to disable)</button>
                            </form>

                            <button type="button" class="btn btn-sm btn-danger" disabled>Delete (irreversible!)</button>
                            <?php
                        } else {
                            ?>
                            <form action="/admin/categories" method="post">
                                <input type="hidden" name="action" value="enable">
                                <input type="hidden" name="category" value="<?=$category->id?>">
                                <button class="btn btn-sm btn-danger">Disabled (click to enable)</button>
                            </form>

                            <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteConfirm-<?=$category->id?>">Delete (irreversible!)</button>
                            <?php
                        }
                        ?>
                    </td>
                </tr>

                <!-- Delete Category -->
                <div class="modal fade" id="deleteConfirm-<?=$category->id?>" tabindex="-1" role="dialog" aria-labelledby="deleteConfirmLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form method="POST" action="/admin/categories">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="deleteConfirmLabel">Delete Category: <small class="text-warning"><?=$category->name?></small></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <div class="modal-body">
                                    Are you sure you want to delete this category? <b class="text-danger">This is IRREVERSIBLE</b>
                                </div>

                                <div class="modal-footer">
                                    <input type="hidden" name="action" value="delete">
                                    <input type="hidden" name="category" value="<?=$category->id?>">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button class="btn btn-danger">Delete Category</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- Edit Category -->
                <div class="modal fade" id="editCategory-<?=$category->id?>" tabindex="-1" role="dialog" aria-labelledby="editCategoryLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form method="POST" action="/admin/categories">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="editCategoryLabel">Update Category <small class="text-warning"><?=$category->name?></small></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <div class="modal-body">
                                    <div class="form-group row">
                                        <label for="categoryName" class="col-sm-3">Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" id="categoryName" class="form-control" name="name" value="<?=$category->name?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="categoryDescription" class="col-sm-3">Description</label>
                                        <div class="col-sm-9">
                                            <input type="text" id="categoryDescription" class="form-control" name="description" value="<?=$category->description?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <input type="hidden" name="action" value="update">
                                    <input type="hidden" name="category" value="<?=$category->id?>">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button class="btn btn-primary">Update Category</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
