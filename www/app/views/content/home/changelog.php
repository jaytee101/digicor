<h3 style="float: left;">Developer Notes <small class="text-muted">code commit messages & changelogs</small></h3>
<img src="https://jaytwitch.visualstudio.com/clients/_apis/build/status/digicor-configurator%20Release%20Build" alt="build status badge" style="float: right; ">

<hr style="clear: both; ">

<?php
foreach ($commitLog as $date => $commits) {
    ?>
    <h2><?=$date?></h2>
    <table class="table table-hover">
    <?php
    foreach ($commits as $commit) {
        echo (!empty($commit['version'])) ? '<tr class="table-secondary">' : '<tr>';
        ?>
            <td scope="col" class="text-muted" style="width: 7rem; "><small><?=$commit['hash']?></small></td>
            <td scope="col">
                <?php
                $output = new \Parsedown();
                echo $output->text($commit['message']);

                if (!empty($commit['description'])) {
                    // $descriptions = explode('-', $commit['description']);
                    echo $output->text($commit['description']);
                }
                ?>
            </td>
            <td scope="col" class="text-right">
                <?=empty($commit['version']) ? '' : '<span class="badge badge-info">' . $commit['version'] . '</span>'?>
            </td>
        </tr>
        <?php
    }
    ?>
    </table>
    <br />
    <?php
}
