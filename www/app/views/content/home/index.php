        </div>
    </div>
</div>

<div id="featureCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#featureCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#featureCarousel" data-slide-to="1"></li>
        <li data-target="#featureCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100 h-100" src="holder.js/100px400?bg=777777&fg=ffffff&text=Slide01" alt="First slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100 h-100" src="holder.js/100px400?bg=777777&fg=ffffff&text=Slide02" alt="Second slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100 h-100" src="holder.js/100px400?bg=777777&fg=ffffff&text=Slide03" alt="Third slide">
        </div>
    </div>
    <a class="carousel-control-prev" href="#featureCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#featureCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<div class="container">
    <div class="row">
        <div class="col">
            <h1>Digicor Configurator</h1>
            <hr>
            <p>
                This is a prototype for the Digicor Configurator - this prototype focuses primarily on
                functionality, as opposed to final graphical design.
            </p>
            <br>
            <br>

            <h3 class="text-warning">Select a Configuration...</h3>
            <hr>
        </div>
    </div>

    <?php
    $count = 0;
    foreach ($configurations as $config) {
        if ($count == 0) {
            echo '<div class="row">';
        }

        if ($count == 3) {
            echo '</div>';
            $count = 0;
            continue;
        }
        ?>
    <div class="col-sm-4">
        <div class="card">
            <h5 class="card-header">
                <?=$config->name?>
            </h5>
            <div class="card-body">
                <p class="card-text">
                    <?=$config->description?>
                </p>
            </div>
            <div class="card-footer">
                <a href="/quotes/<?=$config->id?>" class="btn btn-block btn-lg btn-warning">Get a Quote</a>
            </div>
        </div>

        <br />
    </div>
    <?php
        $count++;
    }
    ?>
</div>