<?php
$groupCount = 1;
$componentCount = 1;
?>

<h1>Manage Configuration: <small class="text-warning"><?=$config->name?></small></h1>

<p>
    Manage the details of the configuration, as well as add sections & groups of products to the configuration for customers to select and choose.
</p>

<div class="row">
    <div class="col-md-2">
        <ul class="nav nav-pills flex-column">
            <li class="nav-item">
                <a href="/admin/configurations/<?=$section->configuration?>" class="nav-link<?=$viewOpts['page']['section'] == 'home' ? ' active' : ''?>">Config Overview</a>
                <a href="/admin/configurations/<?=$section->configuration?>/sections" class="nav-link<?=$viewOpts['page']['section'] == 'sections' ? ' active' : ''?>">Sections, Groups &amp; Components</a>
                <a href="/admin/configurations/<?=$section->configuration?>/reviews" class="nav-link<?=$viewOpts['page']['section'] == 'reviews' ? ' active' : ''?>">Config Reviews</a>
                <a href="/admin/configurations/<?=$section->configuration?>/questions" class="nav-link<?=$viewOpts['page']['section'] == 'reviews' ? ' active' : ''?>">Manage Q &amp; A</a>
            </li>
        </ul>
    </div>

    <div class="col-md-10">
        <form action="/admin/configurations/<?=$section->configuration?>/sections/<?=$section->id?>/edit" method="post">
        <div class="row">
            <div class="col">
                <h2>Edit Section</h2>
                <br />
                <div class="row" id="section-<?=$section->id?>">
                    <div class="col">
                        <div class="card">
                            <div class="card-header">
                                <div class="form-row">
                                    <div class="col-auto">
                                        <h4 class="text-warning">Section:</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="name" placeholder="Section Name" value="<?=$section->name?>" />
                                    </div>
                                    <div class="col-md-1">
                                        <input type="text" class="form-control" name="itemlimit" placeholder="Item Limit" value="<?=$section->itemlimit?>" />
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-check">
                                            <input type="hidden" name="optional" value="0">
                                            <input type="checkbox" name="optional" id="optionalSection" class="form-check-input" value="1"<?=$section->optional == true ? ' checked="checked"' : ''?>>
                                            <label for="optionalSection" class="form-check-label">Optional Section, customer can select nothing</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="hidden" name="hidden" value="0">
                                            <input type="checkbox" name="hidden" id="hiddenSection" class="form-check-input" value="1"<?=$section->hidden == true ? ' checked="checked"' : ''?>>
                                            <label for="hiddenSection" class="form-check-label">Hide, customers can't see this whole section</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <b>Section Description:</b> &nbsp; <small class="text-muted"><em>(You can leave this empty if necessary)</em></small>
                                    <br />
                                    <textarea name="description" class="form-control sectionDescription"><?=$section->description?></textarea>
                                </div>

                                <br />
                                <br />

                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <h5 class="card-header">Product Groups in this Section <button type="button" class="btn btn-primary addNewGroup" data-id="<?=$section->id?>">Add Group to Section</button></h5>
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <?php
                                                        foreach ($groups as $group) {
                                                            ?>
                                                            <div class="row" id="group-<?=$group->id?>">
                                                                <div class="col">
                                                                    <div class="card">
                                                                        <div class="card-body">
                                                                            <div class="row">
                                                                                <div class="col-md-3">
                                                                                    <input type="hidden" name="groups[<?=$group->id?>][id]" value="<?=$group->id?>">
                                                                                    <input type="text" class="form-control" name="groups[<?=$group->id?>][name]" placeholder="Product Group Name" value="<?=$group->name?>" />
                                                                                    <div class="form-check">
                                                                                        <input type="hidden" name="groups[<?=$group->id?>][hidden]" value="0">
                                                                                        <input type="checkbox" name="groups[<?=$group->id?>][hidden]" id="hiddenGroup-<?=$section->id?>" class="form-check-input" value="1"<?=$group->hidden == true ? ' checked="checked"' : ''?>>
                                                                                        <label for="hiddenGroup-<?=$section->id?>" class="form-check-label">Hide Group</label>
                                                                                    </div>
                                                                                    <small class="text-muted">If a group is hidden, all products within the group are automatically selected as part of the Configuration</small>
                                                                                    <br />
                                                                                    <br />
                                                                                    <button type="button" class="btn btn-block btn-primary addComponent" data-group="<?=$group->id?>" data-section="<?=$section->id?>"><i class="fa fa-plus-square"></i> Add Component to Group</button><br />
                                                                                    <button type="button" class="btn btn-block btn-danger removeGroup" data-group="<?=$group->id?>" data-section="<?=$section->id?>"><i class="fa fa-times-circle"></i> Remove Group</button>
                                                                                </div>
                                                                                <div class="col-md-9">
                                                                                    Optional Group Note to Customer:
                                                                                    <textarea name="groups[<?=$group->id?>][description]" class="form-control groupDescription"><?=$group->description?></textarea>
                                                                                </div>
                                                                            </div>
                                                                            <hr>
                                                                            <div class="row">
                                                                                <div class="col">
                                                                                    <small class="text-muted">If marked as a Base Component, that component will be automatically selected when the Customer views the configuration.</small>
                                                                                    <br />
                                                                                    <div class="form-row row" style="border-bottom: 1px solid #ccc; margin-bottom: 5px; ">
                                                                                        <div class="col-md-1"><small>Is Base?</small></div>
                                                                                        <div class="col-md-1"><small>Base Qty</small></div>
                                                                                        <div class="col-md-5"><small>Product Name</small></div>
                                                                                        <div class="col-md-1"><small>Qty Step</small></div>
                                                                                        <div class="col-md-1"><small>Price Override</small></div>
                                                                                        <div class="col-md-1"><small>Hidden?</small></div>
                                                                                        <div class="col-md-1"><small>Edit</small></div>
                                                                                        <div class="col-md-1"><small>Delete</small></div>
                                                                                    </div>
                                                                                    <?php
                                                                                    $components = R::find('config_components', ' `group` = ? AND section = ? AND configuration = ? AND enabled = 1', [ $group->id, $section->id, $config->id ]);
                                                                                    foreach ($components as $comp) {
                                                                                        ?>
                                                                                        <div class="form-row row" id="component-<?=$componentCount?>">
                                                                                            <div class="col-md-1">
                                                                                                <div class="form-check">
                                                                                                    <input type="hidden" name="groups[<?=$group->id?>][components][<?=$componentCount?>][configcompid]" value="<?=$comp->id?>">
                                                                                                    <input type="hidden" name="groups[<?=$group->id?>][components][<?=$componentCount?>][base]" value="0">
                                                                                                    <input type="checkbox" name="groups[<?=$group->id?>][components][<?=$componentCount?>][base]" id="baseComponent-<?=$section->id?>" class="form-check-input" value="1"<?=$comp->base == true ? ' checked="checked"' : ''?>>
                                                                                                    <label for="baseComponent-<?=$section->id?>" class="form-check-label">Base</label>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-1">
                                                                                                <input type="number" class="form-control" name="groups[<?=$group->id?>][components][<?=$componentCount?>][qty]" id="" value="0">
                                                                                            </div>
                                                                                            <div class="col-md-5">
                                                                                                <select name="groups[<?=$group->id?>][components][<?=$componentCount?>][id]" class="form-control">
                                                                                                    <?php
                                                                                                    $parts = R::find('components', ' enabled = 1 ORDER BY name ASC');
                                                                                                    foreach ($parts as $part) {
                                                                                                        ?>
                                                                                                        <option value="<?=$part->id?>"<?=$comp->component == $part->id ? ' selected' : ''?>><?=$part->name?></option>
                                                                                                        <?php
                                                                                                    }
                                                                                                    ?>
                                                                                                </select>
                                                                                            </div>
                                                                                            <div class="col-md-1">
                                                                                                <input type="text" class="form-control" name="groups[<?=$group->id?>][components][<?=$componentCount?>][step]" placeholder="Step Qty" value="<?=$comp->step?>">
                                                                                            </div>
                                                                                            <div class="col-md-1">
                                                                                                <input type="text" class="form-control" name="groups[<?=$group->id?>][components][<?=$componentCount?>][price]" placeholder="Price Override" value="<?=$comp->price?>">
                                                                                            </div>
                                                                                            <div class="col-md-1">
                                                                                                <div class="form-check">
                                                                                                    <input type="hidden" name="groups[<?=$group->id?>][components][<?=$componentCount?>][hidden]" value="0">
                                                                                                    <input type="checkbox" name="groups[<?=$group->id?>][components][<?=$componentCount?>][hidden]" id="hiddenComponent-<?=$section->id?>" class="form-check-input" value="1"<?=$comp->hidden == true ? ' checked="checked"' : ''?>>
                                                                                                    <label for="hiddenComponent-<?=$section->id?>" class="form-check-label">Hide</label>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-1">
                                                                                                <button type="button" class="btn btn-block btn-primary editComponent" data-id="<?=$comp->id?>" data-toggle="modal" data-target="#editComponent">Edit</button>
                                                                                            </div>
                                                                                            <div class="col-md-1">
                                                                                                <button type="button" class="btn btn-block btn-danger removeComponent" data-id="<?=$componentCount?>"><i class="fa fa-trash-alt"></i></button>
                                                                                            </div>
                                                                                        </div>
                                                                                        <?php
                                                                                        $componentCount++;
                                                                                    }
                                                                                    ?>
                                                                                    <div class="components-<?=$group->id?>"></div>
                                                                                    <br />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br />
                                                            <br />
                                                            <?php
                                                            $groupCount++;
                                                        }
                                                        ?>
                                                        <div class="groups-<?=$section->id?>"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <input type="hidden" name="action" value="save">
                <input type="hidden" name="config" value="<?=$section->configuration?>">
                <input type="hidden" name="section" value="<?=$section->id?>">
                <button class="btn btn-success"><i class="fas fa-save"></i> Update Section</button>
                </form>
                <br />
                <hr />
                <br />
            </div>
        </div>
        </form>
    </div>
</div>

<br />
<br />

<!-- Edit Component Modal -->
<div class="modal fade" id="editComponent" tabindex="-1" role="dialog" aria-labelledby="editComponentLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form method="POST" action="/admin/configurations/<?=$config->id?>/sections/<?=$section->id?>/edit" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="editComponentLabel">Edit Component Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="resultBox"></div>
                    <datalist id="categories">
                        <?php
                        foreach ($categories as $category) {
                            ?>
                            <option value="<?=$category->name?>"><?=$category->name?></option>
                            <?php
                        }
                        ?>
                    </datalist>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-danger">Update Component Details</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
function initTinyMCE() {
    tinymce.init({
        selector: '.sectionDescription',
        plugins: 'link lists',
        menubar: false,
        toolbar: 'undo redo | bold italic underline | link unlink | numlist bullist',
        width: '100%'
    });

    tinymce.init({
        selector: '.groupDescription',
        plugins: 'link lists',
        menubar: false,
        toolbar: 'undo redo | bold italic underline | link unlink | numlist bullist',
        width: '100%'
    });
}

initTinyMCE();

// counters
var groupCount     = <?=$groupCount?>;
var componentCount = <?=$componentCount?>;

$(document).on('click', "button.editComponent", function() {
    $.ajax({
        type: "POST",
        url: "/admin/components/view/" + $(this).data('id'),
        data: { id: $(this).data('id') },
        success: function(result) {
            $('.resultBox').html(result);
        },
        error: function(result) {
            alert('error');
        }
    })
});

// add group to section
$(document).on('click', "button.addNewGroup", function() {
    var id   = $(this).data("id");
    console.log("section id: " + id);

    $.ajax({
        type: "POST",
        url: "/admin/configurations/" + <?=$section->configuration?> + "/sections/" + <?=$section->id?> + "/edit",
        data: {
            action: 'ajax',
            call: 'addNewGroup',
            section: id,
            group: groupCount,
            editing: true
        },
        success: function(result) {
            $('.groups-'+id).append(result);
            tinymce.remove();
            initTinyMCE();
            groupCount++;
        },
        error: function(result) {
            alert('Error: addNewGroup to section');
        }
    });
});

// add component to group
$(document).on('click', "button.addComponent", function() {
    var groupid = $(this).data("group");
    var sectionid = $(this).data("section");

    $.ajax({
        type: "POST",
        url: "/admin/configurations/" + <?=$section->configuration?> + "/sections/" + <?=$section->id?> + "/edit",
        data: {
            action: 'ajax',
            call: 'addComponent',
            group: groupid,
            section: sectionid,
            component: componentCount,
            editing: true
        },
        success: function(result) {
            $('.components-'+groupid).append(result);
            tinymce.remove();
            initTinyMCE();
            componentCount++;
        },
        error: function(result) {
            alert('Error: addComponent to group');
        }
    });
});

// remove Section from list
$(document).on('click', 'button.removeSection', function() {
    var id = $(this).data("id");
    $('#section-'+id).remove();
});

// remove Group from Section
$(document).on('click', 'button.removeGroup', function() {
    var id = $(this).data("group");
    $('#group-'+id).remove();
});

// remove Component from Group
$(document).on('click', 'button.removeComponent', function() {
    var id = $(this).data("id");
    $('#component-'+id).remove();
});
</script>
