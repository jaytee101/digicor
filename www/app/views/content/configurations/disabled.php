<div class="row">
    <div class="col">
        <div class="card">
            <h1 class="card-header">Disabled Configurations</h1>
            <div class="card-body">
                <h5 class="card-title text-warning">Instructions</h5>
                <p class="card-text">
                    Create configurations which customers can select, and then
                    configure to their needs to generate their own quotes. 
                    Start with the button on the left; and as each Config is 
                    generated, it will move through each column.
                </p>
            </div>
        </div>

        <br />
    </div>
</div>

<div class="row">
    <div class="col">
        <table class="table table-hover table-striped datatable">
            <thead>
                <tr>
                    <th>Last Updated</th>
                    <th>Configuration Name</th>
                    <th>Price</th>
                    <th>Options</th>
                </tr>
            </thead>

            <tbody>
            <?php
            foreach ($configurations as $config) {
                ?>
                <tr>
                    <td>
                        <?=empty($config->updated) ? date('d/m/Y', $config->created) : date('d/m/Y', $config->updated)?>
                    </td>
                    <td><?=$config->name?></td>
                    <td><?=empty($config->price) ? '0.00' : number_format(($config->price/100), 2, '.', ' ')?></td>
                    <td class="d-flex justify-content-around">
                        <a href="/admin/configurations/<?=$config->id?>" class="btn btn-sm btn-primary">Edit/Manage</a>
                        <button type="button" class="btn btn-sm btn-success enableConfig" data-toggle="modal" data-target="#enableConfiguration" data-id="<?=$config->id?>"><i class="fa fa-redo-alt"></i> Re-enable</button>
                        <button type="button" class="btn btn-sm btn-danger deleteConfig" data-toggle="modal" data-target="#deleteConfiguration" data-id="<?=$config->id?>"><i class="fa fa-trash-alt"></i> Delete</button>
                    </td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<!-- Enable configuration -->
<div class="modal fade" id="enableConfiguration" tabindex="-1" role="dialog" aria-labelledby="enableConfigurationLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="/admin/configurations">
                <div class="modal-header">
                    <h5 class="modal-title" id="enableConfigurationLabel">Re-enable Configuration</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    Are you sure you want to re-enable this Configuration?
                </div>

                <div class="modal-footer">
                    <input type="hidden" name="action" value="enable">
                    <input type="hidden" name="configuration" id="enableConfigId">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-success">Enable Configuration</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Delete configuration -->
<div class="modal fade" id="deleteConfiguration" tabindex="-1" role="dialog" aria-labelledby="deleteConfigurationLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="/admin/configurations">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteConfigurationLabel">DELETE Configuration</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    Are you sure you want to DELETE this Configuration?
                    <br /><br />
                    <b class="text-danger">THIS IS IRREVERSIBLE</b>
                </div>

                <div class="modal-footer">
                    <input type="hidden" name="action" value="delete">
                    <input type="hidden" name="configuration" id="deleteConfigId">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-danger">DELETE Configuration</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
$('.enableConfig').click(function() {
    var id = $(this).data("id");

    $('#enableConfigId').val(id);
});

$('.deleteConfig').click(function() {
    var id = $(this).data("id");

    $('#deleteConfigId').val(id);
});
</script>
