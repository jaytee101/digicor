<h1>Manage Configuration: <small class="text-warning"><?=$config->name?></small></h1>

<p>
    Manage the details of the configuration, as well as add sections & groups of products to the configuration for customers to select and choose.
</p>

<div class="row">
    <div class="col-md-2">
        <ul class="nav nav-pills flex-column">
            <li class="nav-item">
                <a href="/admin/configurations/<?=$config->id?>" class="nav-link<?=$viewOpts['page']['section'] == 'home' ? ' active' : ''?>">Config Overview</a>
                <a href="/admin/configurations/<?=$config->id?>/sections" class="nav-link<?=$viewOpts['page']['section'] == 'sections' ? ' active' : ''?>">Sections, Groups &amp; Components</a>
                <a href="/admin/configurations/<?=$config->id?>/reviews" class="nav-link<?=$viewOpts['page']['section'] == 'reviews' ? ' active' : ''?>">Config Reviews</a>
                <a href="/admin/configurations/<?=$config->id?>/questions" class="nav-link<?=$viewOpts['page']['section'] == 'reviews' ? ' active' : ''?>">Manage Q &amp; A</a>
            </li>
        </ul>
    </div>

    <div class="col-md-10">
            <div class="row">
                <div class="col">
                    <h2>Existing Sections</h2>
                    <br />

                    <table class="table table-hover">
                        <?php
                        foreach ($sections as $section) {
                            ?>
                            <tr>
                                <td class="col-auto">
                                    <a class="btn btn-primary" href="/admin/configurations/<?=$config->id?>/sections/<?=$section->id?>/edit">Edit</button>
                                </td>
                                <td class="col">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <h4><?=empty($section->name) ? '<em>No name provided</em> (ID: ' . $section->id . ')' : '<b>' . $section->name . '</b>'?></h4>
                                            <?php
                                            if (!empty($section->description)) {
                                                ?>
                                                <small class="text-muted"><?=$section->description?></small>
                                                <?php
                                            }

                                            echo $section->itemlimit . ' item(s) in this section across the following groups:<br /><br />';

                                            $groups = R::find('config_groups', ' section = ? AND enabled = 1', [ $section->id ]);
                                            foreach ($groups as $group) {
                                                echo '<h5>';
                                                echo (empty($group->name)) ? '<em>No Group Name</em>' : '<b>' . $group->name . '</b>';
                                                echo '</h5>';

                                                $components = R::find('config_components', ' `group` = ? AND section = ? AND enabled = 1', [ $group->id, $section->id ]);

                                                if (!empty($components)) {
                                                    echo '<ul>';
                                                }

                                                foreach ($components as $component) {
                                                    echo '<li>';

                                                    if (!empty($component->price)) {
                                                        echo '[$' . number_format($component->price, 2) . '] ';
                                                    } else {
                                                        echo '[$' . number_format((\app\Models\Component::get($component->component, 'price') / 100), 2) . '] ';
                                                    }

                                                    echo \app\Models\Component::get($component->component, 'name');
                                                    echo ($component->base == true) ? ' <span class="badge badge-primary">Base Component</span>' : '';
                                                    echo '</li>';
                                                }

                                                if (!empty($components)) {
                                                    echo '</ul>';
                                                }
                                            }
                                            ?>

                                        </div>

                                        <div class="col-md-2 text-right">
                                        <!--
                                            <form action="/admin/configurations/<?=$config->id?>/sections" method="post" id="toggleSection" style="float: left; ">
                                            <?php
                                            if ($section->hidden == false) {
                                                ?>
                                                <input type="hidden" name="action" value="hideSection">
                                                <input type="hidden" name="section" value="<?=$section->id?>">
                                                <button type="button" class="btn btn-success" id="hideSection"><i class="fa fa-eye"></i></button>
                                                <?php
                                            } else {
                                                ?>
                                                <input type="hidden" name="action" value="showSection">
                                                <input type="hidden" name="section" value="<?=$section->id?>">
                                                <button type="button" class="btn btn-secondary" id="showSection"><i class="fa fa-eye-slash"></i></button>
                                                <?php
                                            }
                                            ?>
                                            </form>
                                            <?php
                                            if ($section->optional == false) {
                                                ?>
                                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#makeOptional" data-id="<?=$section->id?>">Required</button>
                                                <?php
                                            } else {
                                                ?>
                                                <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#makeRequired" data-id="<?=$section->id?>">Optional</button>
                                                <?php
                                            }
                                            ?>
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#">Move/Copy</button>
                                        -->
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteSection"><i class="fa fa-trash-alt"></i></button>

                                            <!-- Delete Section -->
                                            <div class="modal fade" id="deleteSection" tabindex="-1" role="dialog" aria-labelledby="deleteSectionLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <form method="POST" action="/admin/configurations/<?=$config->id?>/sections">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="deleteSectionLabel">Delete Section: <?=empty($section->name) ? '<em>No name provided</em> (ID: ' . $section->id . ')' : '<b>' . $section->name . '</b>'?></h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>

                                                            <div class="modal-body text-left">
                                                                <span class="badge badge-danger"><i class="fa fa-lg fa-warning"></i> This is irreversible!</span>
                                                                <p>
                                                                    Are you sure you wish to delete this Section?
                                                                </p>
                                                            </div>

                                                            <div class="modal-footer">
                                                                <input type="hidden" name="action" value="delete">
                                                                <input type="hidden" name="section" value="<?=$section->id?>">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                                <button class="btn btn-danger">Yes, delete this section</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                    <br />
                    <hr />
                    <h2>New Sections <button type="button" class="btn btn-primary addNewSection">Add New Section</button></h2>
                    <br />
                    <form action="/admin/configurations/<?=$config->id?>/sections" method="post">
                    <div class="sections"></div>
                    <br />
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <input type="hidden" name="action" value="save">
                    <input type="hidden" name="config" value="<?=$config->id?>">
                    <button class="btn btn-success"><i class="fas fa-save"></i> Save Configuration Sections</button>
                    </form>
                    <br />
                    <hr />
                    <br />
                </div>
            </div>
    </div>
</div>

<br />
<br />


<script>
function initTinyMCE() {
    tinymce.init({
        selector: '#configDescription',
        plugins: 'link',
        menubar: false,
        toolbar: 'undo redo | bold italic underline | link unlink'
    });

    tinymce.init({
        selector: '.sectionDescription',
        plugins: 'link lists',
        menubar: false,
        toolbar: 'undo redo | bold italic underline | link unlink | numlist bullist',
        width: '100%'
    });

    tinymce.init({
        selector: '.groupDescription',
        plugins: 'link lists',
        menubar: false,
        toolbar: 'undo redo | bold italic underline | link unlink | numlist bullist',
        width: '100%'
    });
}

initTinyMCE();

// counters
var sectionCount   = 0;
var groupCount     = 0;
var componentCount = 0;

$(document).ready(function() {
    $('.configLogTable').DataTable();
});

// add section to configuration
$(document).on('click', "button.addNewSection", function() {
    $.ajax({
        type: "POST",
        url: "/admin/configurations/" + <?=$config->id?> + "/sections",
        data: {
            action: 'ajax',
            call: 'addNewSection',
            section: sectionCount,
            group: groupCount
        },
        success: function(result) {
            $('.sections').append(result);
            tinymce.remove();
            initTinyMCE();
            sectionCount++;
        },
        error: function(result) {
            alert('Error: addNewSection');
        }
    });
});

// add group to section
$(document).on('click', "button.addNewGroup", function() {
    var id   = $(this).data("id");
    var groupName = $('input[name="sections['+id+'][group]['+groupCount+'][name]"').val();

    $.ajax({
        type: "POST",
        url: "/admin/configurations/" + <?=$config->id?> + "/sections",
        data: {
            action: 'ajax',
            call: 'addNewGroup',
            section: id,
            group: groupCount
        },
        success: function(result) {
            $('.groups-'+id).append(result);
            tinymce.remove();
            initTinyMCE();
            groupCount++;
        },
        error: function(result) {
            alert('Error: addNewGroup to section');
        }
    });
});

// add component to group
$(document).on('click', "button.addComponent", function() {
    var groupid = $(this).data("group");
    var sectionid = $(this).data("section");

    $.ajax({
        type: "POST",
        url: "/admin/configurations/" + <?=$config->id?> + "/sections",
        data: {
            action: 'ajax',
            call: 'addComponent',
            group: groupid,
            section: sectionid,
            component: componentCount
        },
        success: function(result) {
            $('.components-'+groupid).append(result);
            tinymce.remove();
            initTinyMCE();
            componentCount++;
        },
        error: function(result) {
            alert('Error: addComponent to group');
        }
    });
});

// remove Section from list
$(document).on('click', 'button.removeSection', function() {
    var id = $(this).data("id");
    $('#section-'+id).remove();
});

// remove Group from Section
$(document).on('click', 'button.removeGroup', function() {
    var id = $(this).data("group");
    $('#group-'+id).remove();
});

// remove Component from Group
$(document).on('click', 'button.removeComponent', function() {
    var id = $(this).data("id");
    $('#component-'+id).remove();
});
</script>
