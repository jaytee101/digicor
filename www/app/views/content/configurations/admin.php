<div class="row">
    <div class="col">
        <div class="card">
            <h1 class="card-header">Configurations</h1>
            <div class="card-body">
                <h5 class="card-title text-warning">Instructions</h5>
                <p class="card-text">
                    Create configurations which customers can select, and then
                    configure to their needs to generate their own quotes. 
                    Start with the button on the left; and as each Config is 
                    generated, it will move through each column.
                </p>
            </div>
        </div>

        <br />
    </div>
</div>

<div class="row">
    <div class="col">
        <table class="table table-hover table-striped datatable">
            <thead>
                <tr>
                    <th>Last Updated</th>
                    <th>Published</th>
                    <th>Configuration Name</th>
                    <th>Price</th>
                    <th>Options</th>
                </tr>
            </thead>

            <tbody>
            <?php
            foreach ($configurations as $config) {
                ?>
                <tr>
                    <td>
                        <?=empty($config->updated) ? date('d/m/Y', $config->created) : date('d/m/Y', $config->updated)?>
                    </td>
                    <td>
                        <?php
                        if (empty($config->published)) {
                            ?>
                            <form action="/admin/configurations" method="post">
                                <input type="hidden" name="action" value="publish">
                                <input type="hidden" name="configuration" value="<?=$config->id?>">
                                <button class="btn btn-sm btn-secondary">Unpublished (click to publish)</button>
                            </form>
                            <?php
                        } else {
                            ?>
                            <form action="/admin/configurations" method="post">
                                <input type="hidden" name="action" value="unpublish">
                                <input type="hidden" name="configuration" value="<?=$config->id?>">
                                <button class="btn btn-sm btn-info"><?=date('d/m/Y', $config->published)?> (click to unpublish)</button>
                            </form>
                            <?php
                        }
                        ?>
                    </td>
                    <td><?=$config->name?></td>
                    <td><?=empty($config->price) ? '0.00' : number_format(($config->price/100), 2, '.', ' ')?></td>
                    <td class="d-flex justify-content-around">
                        <a href="/admin/configurations/<?=$config->id?>" class="btn btn-sm btn-primary">Edit/Manage</a>
                        <button class="btn btn-sm btn-danger disableConfig" data-toggle="modal" data-target="#disableConfiguration" data-id="<?=$config->id?>"><i class="fa fa-trash-alt"></i></button>
                    </td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<!-- Disable configuration -->
<div class="modal fade" id="disableConfiguration" tabindex="-1" role="dialog" aria-labelledby="disableConfigurationLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="/admin/configurations">
                <div class="modal-header">
                    <h5 class="modal-title" id="disableConfigurationLabel">Disable Configuration</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    Are you sure you want to disable this Configuration?
                </div>

                <div class="modal-footer">
                    <input type="hidden" name="action" value="disable">
                    <input type="hidden" name="configuration" id="disableConfigId">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-danger">Disable Configuration</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
$('.disableConfig').click(function() {
    var id = $(this).data("id");

    $('#disableConfigId').val(id);
});
</script>
