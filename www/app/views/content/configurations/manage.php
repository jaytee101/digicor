<h1>Manage Configuration: <small class="text-warning"><?=$config->name?></small></h1>

<p>
    Manage the details of the configuration, as well as add sections & groups of products to the configuration for customers to select and choose.
</p>

<div class="row">
    <div class="col-md-2">
        <ul class="nav nav-pills flex-column">
            <li class="nav-item">
                <a href="/admin/configurations/<?=$config->id?>" class="nav-link<?=$viewOpts['page']['section'] == 'home' ? ' active' : ''?>">Config Overview</a>
                <a href="/admin/configurations/<?=$config->id?>/sections" class="nav-link<?=$viewOpts['page']['section'] == 'sections' ? ' active' : ''?>">Sections, Groups &amp; Components</a>
                <a href="/admin/configurations/<?=$config->id?>/reviews" class="nav-link<?=$viewOpts['page']['section'] == 'reviews' ? ' active' : ''?>">Config Reviews</a>
                <a href="/admin/configurations/<?=$config->id?>/questions" class="nav-link<?=$viewOpts['page']['section'] == 'reviews' ? ' active' : ''?>">Manage Q &amp; A</a>
            </li>
        </ul>
    </div>

    <div class="col-md-10">
        <div class="row row-eq-height">
            <div class="col-md-3">
                <div class="card h-100">
                    <h5 class="card-header">Configuration Category</h5>
                    <div class="card-body">
                        //
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card h-100">
                    <h5 class="card-header">Configuration Tags</h5>
                    <div class="card-body">
                        //
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card h-100">
                    <h5 class="card-header">Reviews Summary</h5>
                    <div class="card-body">
                        <b>Reviewed Count:</b> <?=R::count('config_reviews', ' configuration = ?', [ $config->id ])?>
                        <br />
                        <b>Average Review Score:</b> 0
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card h-100">
                    <h5 class="card-header">Configuration Structure</h5>
                    <div class="card-body">
                        <b>Sections:</b> <?=R::count('config_sections', ' configuration = ?', [ $config->id ])?>
                        <br />
                        <b>Groups:</b> <?=R::count('config_groups', ' configuration = ?', [ $config->id ])?>
                        <br />
                        <b>Components:</b> <?=R::count('config_components', ' configuration = ?', [ $config->id ])?>
                    </div>
                </div>
            </div>
        </div>

        <br />

        <div class="row">
            <div class="col">
                <form action="/admin/configurations/<?=$config->id?>" method="post">
                    <div class="card">
                        <h5 class="card-header">Edit Configuration Details</h5>
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="configName" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="name" id="configName" placeholder="Configuration name..." value="<?=$config->name?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="configSKU" class="col-sm-2 col-form-label">Item Code/SKU</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="name" id="configSKU" placeholder="Configuration item code/sku..." value="<?=$config->sku?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="configDescription" class="col-sm-2 col-form-label">Description</label>
                                <div class="col-sm-10">
                                    <textarea type="text" class="form-control" id="configDescription" name="description" placeholder="Configuration description..."><?=$config->description?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="configPrice" class="col-sm-2 col-form-label">Base Price</label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">$</div>
                                        </div>
                                        <input type="text" class="form-control" id="configPrice" name="price" placeholder="<?=($config->autoprice == true) ? 'Auto-calculated price' : '000.00'?>" value="<?=($config->autoprice == false) ? ($config->price/100) : null?>">
                                    </div>
                                    <br />
                                    <div class="form-group form-check">
                                        <input type="hidden" name="autoprice" value="0">
                                        <input type="checkbox" class="form-check-input" id="autoprice" name="autoprice" value="1"<?=$config->autoprice == true ? ' checked="checked"' : ''?>>
                                        <label class="form-check-label" for="autoprice">Auto-calculate price using components</label>
                                        <br />
                                        <small class="text-muted">The highest priced component of each section is used to auto-calculate</small>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2"></div>
                                <div class="col-md-6">
                                    <input type="hidden" name="action" value="update">
                                    <input type="hidden" name="configuration" value="<?=$config->id?>">
                                    <button class="btn btn-primary">Update Configuration Details</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<br />

<div class="row">
    <div class="col">
        <h3>Configuration Activity Log</h3>
        <table class="table table-hover table-striped configLogTable">
            <thead>
                <tr>
                    <th>Time</th>
                    <th>Activity</th>
                    <th>User</th>
                </tr>
            </thead>

            <tbody>
            <?php
            foreach ($logs as $log) {
                ?>
                <tr>
                    <td>
                        <span style="display: none; "><?=$log->time?></span>
                        <?=date('d/m/Y H:i:s', $log->time)?>
                    </td>
                    <td>
                        <?=$log->message?>
                    </td>
                    <td>
                        <?=\app\Models\User::getByUuid($log->user, 'name')?>
                    </td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<br />
<br />

<script>
function initTinyMCE() {
    tinymce.init({
        selector: '#configDescription',
        plugins: 'link',
        menubar: false,
        toolbar: 'undo redo | bold italic underline | link unlink'
    });

    tinymce.init({
        selector: '.sectionDescription',
        plugins: 'link lists',
        menubar: false,
        toolbar: 'undo redo | bold italic underline | link unlink | numlist bullist',
        width: '100%'
    });

    tinymce.init({
        selector: '.groupDescription',
        plugins: 'link lists',
        menubar: false,
        toolbar: 'undo redo | bold italic underline | link unlink | numlist bullist',
        width: '100%'
    });
}

initTinyMCE();

// counters
var sectionCount   = 0;
var groupCount     = 0;
var componentCount = 0;

$(document).ready(function() {
    $('.configLogTable').DataTable();
});

// add section to configuration
$(document).on('click', "button.addNewSection", function() {
    $.ajax({
        type: "POST",
        url: "/admin/configurations/" + <?=$config->id?>,
        data: {
            action: 'ajax',
            call: 'addNewSection',
            section: sectionCount,
            group: groupCount
        },
        success: function(result) {
            $('.sections').append(result);
            tinymce.remove();
            initTinyMCE();
            sectionCount++;
        },
        error: function(result) {
            alert('Error: addNewSection');
        }
    });
});

// add group to section
$(document).on('click', "button.addNewGroup", function() {
    var id   = $(this).data("id");
    var groupName = $('input[name="sections['+id+'][group]['+groupCount+'][name]"').val();

    $.ajax({
        type: "POST",
        url: "/admin/configurations/" + <?=$config->id?>,
        data: {
            action: 'ajax',
            call: 'addNewGroup',
            section: id,
            group: groupCount
        },
        success: function(result) {
            $('.groups-'+id).append(result);
            tinymce.remove();
            initTinyMCE();
            groupCount++;
        },
        error: function(result) {
            alert('Error: addNewGroup to section');
        }
    });
});

// add component to group
$(document).on('click', "button.addComponent", function() {
    var groupid = $(this).data("group");
    var sectionid = $(this).data("section");

    $.ajax({
        type: "POST",
        url: "/admin/configurations/" + <?=$config->id?>,
        data: {
            action: 'ajax',
            call: 'addComponent',
            group: groupid,
            section: sectionid,
            component: componentCount
        },
        success: function(result) {
            $('.components-'+groupid).append(result);
            tinymce.remove();
            initTinyMCE();
            componentCount++;
        },
        error: function(result) {
            alert('Error: addComponent to group');
        }
    });
});

// remove Section from list
$(document).on('click', 'button.removeSection', function() {
    var id = $(this).data("id");
    $('#section-'+id).remove();
    console.log('removed section-'+id);
});

// remove Group from Section
$(document).on('click', 'button.removeGroup', function() {
    var id = $(this).data("group");
    $('#group-'+id).remove();
    console.log('removed group-'+id);
});

// remove Component from Group
$(document).on('click', 'button.removeComponent', function() {
    var id = $(this).data("id");
    $('#component-'+id).remove();
    console.log('removed component-'+id);
});
</script>
