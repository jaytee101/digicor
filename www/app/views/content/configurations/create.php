<div class="row">
    <div class="col">
        <div class="card">
            <h1 class="card-header">Create Configuration</h1>
            <div class="card-body">
                <div class="card-text">
                    <form action="/admin/configurations" method="post">
                        <div class="form-group row">
                            <label for="configName" class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="name" id="configName" placeholder="Configuration name...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="configName" class="col-sm-2 col-form-label">Item Code/SKU</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="sku" placeholder="Configuration SKU or Item Number...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="configDescription" class="col-sm-2 col-form-label">Description</label>
                            <div class="col-sm-10">
                                <textarea type="text" class="form-control" id="configDescription" name="description" placeholder="Configuration description..."></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="configPrice" class="col-sm-2 col-form-label">Base Price</label>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">$</div>
                                    </div>
                                    <input type="text" class="form-control" id="configPrice" name="price" placeholder="123.45">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-10">
                                <input type="hidden" name="action" value="create">
                                <button class="btn btn-primary">Create New Config</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
tinymce.init({
    selector: '#configDescription',
    plugins: 'link',
    menubar: false,
    toolbar: 'undo redo | bold italic underline | link unlink'
});
</script>
