<div class="row">
    <div class="col">
        <div class="card">
            <h1 class="card-header">Customer Management</h1>
            <div class="card-body">
                <h5 class="card-title text-warning">Instructions</h5>
                <p class="card-text">
                    Customers are just that - your customers, they have no access to the back-end system; but they can save their quotes, and send them on to
                    other people. The email data is captured, and analysed for better use.
                </p>
            </div>
        </div>

        <br />
    </div>
</div>

<div class="row">
    <div class="col">
        <table class="table table-hover table-striped customerTable">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Joined</th>
                    <th>Last&nbsp;Online</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($users as $user) {
                    ?>
                    <tr>
                        <td><?=$user->name?></td>
                        <td><?=$user->email?></td>
                        <td><?=date('d/m/Y H:i:s', $user->created)?></td>
                        <td><?=date('d/m/y H:i:s', $user->lastonline)?></td>
                        <td><a href="/admin/customers/<?=$user->id?>" class="btn btn-primary">Manage User</a></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>

<script>
$('.customerTable').DataTable();
</script>
