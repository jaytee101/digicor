<?php
$conversion = empty($_SESSION['user']['location']['conversion']) ? 1 : $_SESSION['user']['location']['conversion'];
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-9">
            <h2><?=$config->name?></h2>
            <p class="text-muted"><em><?=$config->description?></em></p>
        </div>

        <div class="col-md-3">
            <button class="btn btn-block btn-success text-left"><i class="fas fa-comment-dollar"></i> Get Quote</button>
            <br />
            <button class="btn btn-block btn-info text-left"><i class="far fa-envelope"></i> Email Config</button>
        </div>
    </div>
    <div class="row">
        <div class="col"><hr></div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <form action="/quotes/<?=$config->id?>" method="post" class="form-horizontal" id="cart">
            <?php
            // configuration sections...
            foreach ($sections as $section) {
                // determine if the section is a multiple selection section (checkboxes) or not (radioboxes)
                $multiSelection = ($section->itemlimit > 1) ? true : false;
                ?>
                <div class="row">
                    <div class="col">
                        <h4 class="text-left text-warning"><b><?=$section->name?></b></h4>
                    </div>
                    <div class="col">
                        <h5 class="text-right text-warning">Max <?=$section->itemlimit?> Item<?=($section->itemlimit != 1) ? 's' : ''?></h5>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-2">
                        <img src="holder.js/100px160" alt="section icon" class="rounded img-fluid">
                        <br />
                        <div id="section-<?=$section->id?>-count" data-limit="<?=$section->itemlimit?>"></div>
                    </div>
                    <div class="col-sm-10">
                        <?php
                        if (!empty($section->description)) {
                            ?>
                            <div class="card">
                                <div class="card-body">
                                    <?=$section->description?>
                                </div>
                            </div>

                            <br />
                            <?php
                        }
                        ?>

                        <?php
                        // component groups in the section
                        $groups = R::find('config_groups', ' configuration = ? AND section = ? AND enabled = 1', [ $config->id, $section->id ]);

                        foreach ($groups as $group) {
                            if ($group->hidden == true) {
                                continue;
                            }
                            ?>
                            <div class="card border-secondary">
                                <div class="card-header"><?=$group->name?></div>
                                <div class="card-body">
                                <?php
                                if (!empty($group->description)) {
                                    echo '<small><em>' . $group->description . '</em></small><hr />';
                                }
                                $products = R::find('config_components', ' configuration = ? AND section = ? AND `group` = ? AND enabled = 1', [ $config->id, $section->id, $group->id ]);
                                echo '<fieldset class="form-group" id="group-' . $group->id . '">';
                                foreach ($products as $product) {
                                    $component = R::load('components', $product->component);
                                    ?>
                                    <select
                                        name="<?=$product->id?>"
                                        id="qty-<?=$product->id?>"
                                        style="float: left; width: 2.5rem; ">
                                        <option value="0" selected>0</option>
                                        <?php
                                        for ($i = $product->step; $i <= $section->itemlimit; $i += $product->step) {
                                            ?>
                                            <option value="<?=$i?>"<?=(($i == $product->qty) && ($product->base == true)) ? ' selected' : ''?>><?=$i?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>

                                    <div class="form-check" style="margin-left: 2.5rem; ">
                                        <?php

                                        // add base product label
                                        if ($product->base == true) {
                                            $basePrice = empty($product->price) ? $component->price : $product->price;
                                        }

                                        $price = empty($product->price) ? $component->price : $product->price;

                                        // calculate component price offset
                                        $offsetPrice = ($basePrice - $price) * $conversion;

                                        echo $component->name;

                                        // display price
                                        echo '[ <span class="text-muted">' . (number_format(($component->price / 100), 2)) . '</span> ] ';

                                        // show base product label
                                        if ($product->base == true) {
                                            ?>
                                            <span class="badge badge-secondary">base</span>
                                            <?php
                                        }

                                        // display price offset
                                        if (!empty($offsetPrice)) {
                                            echo '[ <span class="text-' . (($offsetPrice > 0) ? 'success' : 'danger') . '">';
                                            echo (($offsetPrice > 0) ? ' -$' . (number_format(($offsetPrice / 100), 2)) : ' +$' . (number_format(($offsetPrice / 100) * -1, 2)));
                                            echo '</span> ]';
                                        }
                                        ?>
                                    </div>

                                    <div class="clearfix"></div>
                                    <?php
                                }
                                echo '</fieldset>';
                                ?>
                                </div>
                            </div>
                            <br />
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <br>
                <hr>
                <?php
            }
            ?>
            </form>
        </div>

        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Quote Summary</h4>
                    <h6 class="card-subtitle mb-2 text-muted">Total cost of this configuration...</h6>
                </div>

                <div class="card-body">
                    <table class="table table-condensed table-hover table-striped" id="cartList"></table>
                </div>

                <div class="card-body bg-primary text-white">
                    <div class="row">
                        <div class="col">
                            <?php
                            if ($_SESSION['user']['location']['country'] == 'AU') {
                                ?>
                                <img src="/public/img/au.svg" alt="" style="height: 25px; "> AUD
                                <?php
                            } else {
                                ?>
                                <img src="/public/img/nz.svg" alt="" style="height: 25px; "> NZD
                                <?php
                            }
                            ?>
                        </div>
                        <div class="col text-right">
                            <?php
                            $totalPrice = (!empty($config->price)) ? $config->price : '0.00';
                            // number_format($totalPrice * $conversion, 2)
                            ?>
                            <h4 style="padding-top: 0; "><b>$<span id="totalPrice"></span></b></h4>
                        </div>
                    </div>
                </div>

                <div class="card-body" id="testoutput">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col" id="feedback"></div>
</div>

<script src="/public/js/cart.js?=<?=uniqid()?>"></script>

<script>
var startPrice;

function getCart() {
    myCart = {
        "configId": <?=$config->id?>,
        "cart": $('#cart').serializeArray()
    };

    return myCart;
}

startPrice = <?=$config->price?>;

$('#totalPrice').html(parseFloat((startPrice / 100).toFixed(2)));

$(document).on('click', "input.product",
    function() {
        updateCart(getCart());
    }
);

$(document).on('change', '[id^=qty-]',
    function() {
        updateCart(getCart());
    }
);
</script>