<div class="row">
    <div class="col">
        <div class="card">
            <h1 class="card-header">Customer Management</h1>
            <div class="card-body">
                <h5 class="card-title text-warning">Instructions</h5>
                <p class="card-text">
                    Customers are just that - your customers, they have no access to the back-end system; but they can save their quotes, and send them on to
                    other people. The email data is captured, and analysed for better use.
                </p>
            </div>
        </div>

        <br />
    </div>
</div>

<div class="row">
    <div class="col">
        <table class="table table-hover table-striped userTable">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Joined</th>
                    <th>Last&nbsp;Online</th>
                    <th>Quotes</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($customers as $customer) {
                    ?>
                    <tr>
                        <td><?=$customer->name?></td>
                        <td><?=$customer->email?></td>
                        <td><?=date('d/m/Y H:i:s', $customer->created)?></td>
                        <td><?=date('d/m/y H:i:s', $customer->lastonline)?></td>
                        <td><?=R::count('quotes', ' user = ? AND enabled = 1', [ $customer->id ])?></td>
                        <td><a href="/admin/customers/<?=$customer->id?>" class="btn btn-primary">Manage Customer</a></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>

<script>
$('.userTable').DataTable();
</script>
