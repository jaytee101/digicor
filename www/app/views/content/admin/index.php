<div class="container-fluid">
    <div class="row">
        <div class="col">
            <ul>
                <li>
                    <b class="text-warning">Configuration</b> - this is the entire Configuration package, visible to customers. Configurations consist of:
                    <ul>
                        <li>
                            <b class="text-warning">Section(s)</b> - these are categories of components usually, Rules can apply to Sections
                            <ul>
                                <li>
                                    <b class="text-warning">Component(s)</b> - Sections contain base/optional components, and have Rules as well
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
