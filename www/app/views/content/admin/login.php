<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Please Login...</h5>
                    <form action="/admin" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" name="login" placeholder="Email Address">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="Password">
                        </div>
                        <div class="form-group text-center">
                            <input type="hidden" name="action" value="login">
                            <button class="btn btn-primary">
                                Login
                            </button>

                            <!--
                            <a href="/admin/forgot" class="btn btn-default">
                                <small>Forgot Password</small>
                            </a>
                            -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>