<div class="row">
    <div class="col">
        <div class="card">
            <h1 class="card-header">Manually Create Components</h1>
            <div class="card-body">
                <h5 class="card-title text-warning">Instructions</h5>
                <p class="card-text">
                    Through this form, you can manually enter a part/product (components) into the database. The Category can be applied by simply typing the first letter,
                    and then searching through the list for the category you need. If you don't find it, you can always type it up and a new category will be
                    created for this component and future components.
                </p>
            </div>
        </div>

        <br />
    </div>
</div>

<div class="row">
    <div class="col">
        <form action="/admin/components" method="post" class="form-horizontal">
            <div class="form-group row">
                <label for="category" class="col-sm-1 col-form-label">Category</label>
                <div class="col-sm-2">
                    <input type="text" id="category" class="form-control flexdatalist" list="categories" data-min-length="1" name="category">
                    <datalist id="categories">
                        <?php
                        foreach ($categories as $category) {
                            ?>
                            <option value="<?=$category->name?>"><?=$category->name?></option>
                            <?php
                        }
                        ?>
                    </datalist>
                </div>
            </div>
            <div class="form-group row">
                <label for="price" class="col-sm-1 col-form-label">Price</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" id="price" name="price">
                    <small class="form-text text-muted">Price in cents (e.g. $99.20 = 9920)</small>
                </div>
            </div>
            <div class="form-group row">
                <label for="itemcode" class="col-sm-1 col-form-label">SKU</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" id="itemcode" name="itemcode">
                </div>
            </div>
            <div class="form-group row">
                <label for="itemcode" class="col-sm-1 col-form-label">Name</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="name" name="name">
                </div>
            </div>
            <div class="form-group row">
                <label for="description" class="col-sm-1 col-form-label">Description</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="description" name="description">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-1 col-form-label">Image</div>
                <div class="col-sm-4">
                    <?php
                    if (!empty($component->image)) {
                        ?>
                        <img src="<?=$component->image?>" alt="" style="height: 250px; " />
                        <br />
                        <br />
                        <?php
                    }
                    ?>
                    <input type="file" name="image" id="" class="form-control-file">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-1 col-form-label">Hide Price</div>
                <div class="col-sm-1">
                    <div class="form-check">
                        <input type="hidden" name="hidden">
                        <input type="checkbox" name="hidden" id="hiddenPrice" class="form-check-input">
                        <label for="hiddenPrice" class="form-check-label">Yes</label>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                    <div class="col-sm-1 col-form-label"></div>
                    <div class="col-sm-2">
                        <input type="hidden" name="action" value="create">
                        <button class="btn btn-primary">Create New Component</button>
                    </div>
            </div>
        </form>
    </div>
</div>