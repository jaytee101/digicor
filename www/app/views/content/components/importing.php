<?php
// remove column titles
$fieldTitles = array_shift($dataArray);
?>

<div class="row">
    <div class="col">
        <h1>File Import Processing</h1>
        <hr>
        <p>
            We've detected <span class="text-warning"><?=count($dataArray)?></span> products in the file
            [ <span class="text-info"><?=$oldFilename?></span> ]
        </p>
        <hr>
        <form action="/admin/components/import" method="post">
            <div class="form-row">
                <div class="col-sm-2"><b>Category</b></div>
                <div class="col-sm-2"><b>SKU</b></div>
                <div class="col-sm-8"><b>Name</b></div>
                <div class="col-sm-1"><b>Price</b></div>
                <div class="col-sm-1"></div>
            </div>
            <div class="row">
                <div class="col"><hr /></div>
            </div>
            <?php
            $itemCount = 0;
            foreach ($dataArray as $preComponent) {
                ?>
                <div class="form-row" id="row-<?=$itemCount?>" style="margin-bottom: 1rem; ">
                    <div class="col-md-2">
                        <input type="text" class="form-control flexdatalist" list="category" placeholder="Type to search or create a new one" data-min-length="0" name="components[<?=$itemCount?>][category]">
                    </div>
                    <div class="col-md-2">
                        <input type="text" class="form-control" name="components[<?=$itemCount?>][itemcode]" value="<?=htmlentities($preComponent[3])?>">
                    </div>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="components[<?=$itemCount?>][name]" value="<?=htmlentities($preComponent[0])?>">
                        <?php
                        // show an image if there is one
                        if (!empty($preComponent[8])) {
                            echo '<br />';
                            echo '<img src="/uploads/' . $preComponent[8] . '">';
                        }
                        ?>
                        <input type="hidden" name="components[<?=$itemCount?>][picture]" value="<?='/uploads/' . $preComponent[8]?>">
                    </div>
                    <div class="col-md-1">
                        <input type="text" class="form-control" name="components[<?=$itemCount?>][price]" value="<?=htmlentities($preComponent[5])?>">
                    </div>
                    <div class="col-md-1">
                        <button type="button" class="btn btn-danger deleteImport" data-id="<?=$itemCount?>"><i class="fa fa-trash-alt"></i> Delete Row</button>
                    </div>
                </div>
                <?php
                $itemCount++;
            }
            ?>

            <div class="row">
                <div class="col">
                    <input type="hidden" name="action" value="import">
                    <button class="btn btn-primary btn-lg">Import Products</button>
                    <br />
                    <br />

                    <datalist id="category">
                    <?php
                    foreach ($categories as $category) {
                        ?>
                        <option value="<?=$category->name?>"><?=$category->name?></option>
                        <?php
                    }
                    ?>
                    </datalist>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
$('.deleteImport').click(function() {
    var id = $(this).data("id");
    $('#row-' + id).remove();
});
</script>
