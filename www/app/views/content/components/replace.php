<div class="row">
    <div class="col">
        <div class="card">
            <h1 class="card-header">EOL Replacement</h1>
            <div class="card-body">
                <h5 class="card-title text-warning">Instructions</h5>
                <p class="card-text">
                    Sometimes, parts go EOL and need to be switched out - by going through this form, you can swap one product for another
                    across <b>ALL CONFIGURATIONS</b> on the configurator
                </p>
            </div>
        </div>

        <br />
    </div>
</div>