<div class="row">
    <div class="col">
        <div class="card">
            <h1 class="card-header">Components</h1>
            <div class="card-body">
                <h5 class="card-title text-warning">Instructions</h5>
                <p class="card-text">
                    You can edit all the properties of a Component here from this section. The Create a Component button, lets you manually
                    create a new Component listing manually. Alternatively, you can <a href="/admin/import">import</a> components.
                </p>
            </div>
        </div>

        <br />
    </div>
</div>

<div class="row">
    <div class="col">
        <table class="table table-hover table-striped datatable">
            <thead>
                <tr>
                    <th class="w-10">Category</th>
                    <th class="w-20">SKU</th>
                    <th class="w-30">Name</th>
                    <th class="w-10">Image</th>
                    <th class="w-10">Price</th>
                    <th class="w-20">Options</th>
                </tr>
            </thead>

            <tbody>
            <?php
            foreach ($components as $component) {
                ?>
                <tr>
                    <td><span style="white-space: nowrap; "><?=\app\Models\Category::get($component->categoryId, 'name')?></span></td>
                    <td><span style="white-space: nowrap; "><?=$component->itemcode?></span></td>
                    <td><?=$component->name?></td>
                    <td><img src="<?=$component->image?>" style="height: 64px; " /></td>
                    <td><?=empty($component->price) ? '0.00' : number_format(($component->price/100), 2, '.', '')?></td>
                    <td class="d-flex justify-content-around">
                        <button class="btn btn-small btn-primary editComponentButton" data-toggle="modal" data-target="#editComponent" data-id="<?=$component->id?>" style="margin-right: 1rem; ">Edit</button>
                        <button class="btn btn-small btn-danger disableComponentButton" data-toggle="modal" data-target="#disableComponent" data-id="<?=$component->id?>"><i class="fa fa-trash-alt"></i></button>
                    </td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<!-- Edit Component Modal -->
<div class="modal fade" id="editComponent" tabindex="-1" role="dialog" aria-labelledby="editComponentLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form method="POST" action="/admin/components" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="editComponentLabel">Edit Component Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="resultBox"></div>
                    <datalist id="categories">
                        <?php
                        foreach ($categories as $category) {
                            ?>
                            <option value="<?=$category->name?>"><?=$category->name?></option>
                            <?php
                        }
                        ?>
                    </datalist>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-danger">Update Component Details</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Disable Component Modal -->
<div class="modal fade" id="disableComponent" tabindex="-1" role="dialog" aria-labelledby="disableComponentLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="/admin/components">
                <div class="modal-header">
                    <h5 class="modal-title" id="disableComponentLabel">Disable Component</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <p>
                        Are you sure you want to disable this component?
                    </p>

                    <p>
                        By disabling this component, it's not going to be accessible for any future
                        configurations.
                    </p>
                </div>

                <div class="modal-footer">
                    <input type="hidden" name="action" value="disable">
                    <input type="hidden" name="component" id="component">

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-danger">Disable</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
$('.editComponentButton').click(function() {
    $.ajax({
        type: "POST",
        url: "/admin/components/view/" + $(this).data('id'),
        data: { id: $(this).data('id') },
        success: function(result) {
            $('.resultBox').html(result);
        },
        error: function(result) {
            alert('error');
        }
    })
});

$('.disableComponentButton').click(function() {
    var id = $(this).data('id');
    $('#component').val(id);
});
</script>
