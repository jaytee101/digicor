<div class="row">
    <div class="col">
        <div class="card">
            <h1 class="card-header"><span class="text-danger">Disabled</span> Components</h1>
            <div class="card-body">
                <h5 class="card-title text-warning">Instructions</h5>
                <p class="card-text">
                    Disabled components are listed here, in case they need to be re-enabled. <em>Truly deleting</em> a Component is <b class="text-danger">irreversible</b>
                    and once done - you'll have to either manually add the product in again, or it will be re-added when a Component is part of another file import.
                </p>
            </div>
        </div>

        <br />
    </div>
</div>

<div class="row">
    <div class="col">
        <table class="table table-hover table-striped datatable">
            <thead>
                <tr>
                    <th>Category</th>
                    <th>SKU</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Options</th>
                </tr>
            </thead>

            <tbody>
            <?php
            foreach ($components as $component) {
                ?>
                <tr>
                    <td><?=\app\Models\Category::get($component->categoryId, 'name')?></td>
                    <td><span style="white-space: nowrap; "><?=$component->itemcode?></span></td>
                    <td><?=$component->name?></td>
                    <td><?=empty($component->price) ? '0.00' : number_format(($component->price/100), 2, '.', ' ')?></td>
                    <td class="d-flex justify-content-around">
                        <form action="/admin/components" method="post">
                            <input type="hidden" name="action" value="enable">
                            <input type="hidden" name="component" value="<?=$component->id?>">
                            <button class="btn btn-small btn-success">Re-Enable</button>
                        </form>
                    </td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<!-- Edit Component Modal -->
<div class="modal fade" id="editComponent" tabindex="-1" role="dialog" aria-labelledby="editComponentLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form method="POST" action="/admin/components">
                <div class="modal-header">
                    <h5 class="modal-title" id="editComponentLabel">Edit Component Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="resultBox"></div>
                    <datalist id="categories">
                        <?php
                        foreach ($categories as $category) {
                            ?>
                            <option value="<?=$category->name?>"><?=$category->name?></option>
                            <?php
                        }
                        ?>
                    </datalist>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-danger">Update Component Details</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Disable Component Modal -->
<div class="modal fade" id="disableComponent" tabindex="-1" role="dialog" aria-labelledby="disableComponentLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="/admin/components">
                <div class="modal-header">
                    <h5 class="modal-title" id="disableComponentLabel">Disable Component</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <p>
                        Are you sure you want to disable this component?
                    </p>

                    <p>
                        By disabling this component, it's not going to be accessible for any future
                        configurations.
                    </p>
                </div>

                <div class="modal-footer">
                    <input type="hidden" name="action" value="disable">
                    <input type="hidden" name="component" id="component">

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-danger">Disable</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
$('.editComponentButton').click(function() {
    $.ajax({
        type: "POST",
        url: "/admin/components/view/" + $(this).data('id'),
        data: { id: $(this).data('id') },
        success: function(result) {
            console.log(result);
            $('.resultBox').html(result);
        },
        error: function(result) {
            alert('error');
        }
    })
});

$('.disableComponentButton').click(function() {
    var id = $(this).data('id');
    $('#component').val(id);
});
</script>
