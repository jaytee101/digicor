<div class="row">
    <div class="col">
        <div class="card">
            <h1 class="card-header">Import/Update Components</h1>
            <div class="card-body">
                <h5 class="card-title text-warning">Instructions</h5>
                <p class="card-text">
                    Excel files are <b>the preferred way</b> of importing products, they are fully
                    compatible with the configurator's backend - and allow for product specific image importing too!
                </p>
                <p class="card-text">
                    To update a component (or bulk update them) simply upload your Excel file anyway - and if the SKU exists,
                    the item will take the updated values into the database!
                </p>
            </div>
        </div>

        <br />
    </div>
</div>

<div class="row">
    <div class="col">
        <h2>Multi-file upload...</h2>
        <p>
            Currently a work in progress - this <b>does not</b> work yet - the dropzone library I've installed isn't so great, so I'll
            be making something from scratch
        </p>
        <div id="uploadComponents" class="dropzone">
        </div>

        <script>
            Dropzone.autoDiscover = false;

            $('#uploadComponents').dropzone({
                url: "/admin/components/import",
            });
        </script>
    </div>
</div>
