
<ul class="nav nav-pills flex-column">
    <li class="nav-item">
        <a href="/admin/customers" class="nav-link<?=$viewOpts['sidebar']['section'] == 'home' ? ' active' : ''?>">Customers Listing</a>
        <a href="/admin/customers/disabled" class="nav-link<?=$viewOpts['sidebar']['section'] == 'disabled' ? ' active' : ''?>">Manage Disabled Customers</a>
    </li>
</ul>
