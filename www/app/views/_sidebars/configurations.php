
<ul class="nav nav-pills flex-column">
    <li class="nav-item">
        <a href="/admin/configurations" class="nav-link<?=$viewOpts['sidebar']['section'] == 'home' ? ' active' : ''?>">Configurations Listing</a>
        <a href="/admin/configurations/create" class="nav-link<?=$viewOpts['sidebar']['section'] == 'create' ? ' active' : ''?>">Create a New Configuration</a>
        <a href="/admin/configurations/disabled" class="nav-link<?=$viewOpts['sidebar']['section'] == 'disabled' ? ' active' : ''?>">Manage Disabled Configurations</a>
    </li>
</ul>
