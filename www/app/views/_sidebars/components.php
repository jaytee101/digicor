<div class="row">
    <div class="col">
        <ul class="nav nav-pills flex-column">
            <li class="nav-item">
                <a href="/admin/components" class="nav-link<?=$viewOpts['sidebar']['section'] == 'home' ? ' active' : ''?>">Manage Components</a>
            </li>
            <li class="nav-item">
                <a href="/admin/components/create" class="nav-link<?=$viewOpts['sidebar']['section'] == 'create' ? ' active' : ''?>">Create a Component</a>
            </li>
            <li class="nav-item">
                <a href="/admin/components/import" class="nav-link<?=$viewOpts['sidebar']['section'] == 'import' ? ' active' : ''?>">Import a File</a>
            </li>
            <li class="nav-item">
                <a href="/admin/components/replace" class="nav-link<?=$viewOpts['sidebar']['section'] == 'replace' ? ' active' : ''?>">EOL Replacement</a>
            </li>
        </ul>
        <br />
        <br />
    </div>
</div>

<?php
if ($viewOpts['sidebar']['section'] == 'import') {
    ?>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header text-white bg-primary">Import/Update Components</div>
                <div class="card-body">
                    <form action="/admin/components/import" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="file" name="upload" id="" class="form-control-file">
                        </div>

                        <div class="form-group">
                            <input type="hidden" name="action" value="upload">
                            <button type="submit" class="btn btn-secondary">Upload File</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php
}

if ($viewOpts['sidebar']['section'] == 'home') {
    ?>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Category Filter</h5>
                    <?php
                    if ($disabled == true) {
                        ?>
                        <a href="/admin/components" class="text-muted">View Enabled Components</a>
                        <?php
                    } else {
                        ?>
                        <a href="/admin/components/disabled" class="text-danger">View Disabled Components</a>
                        <?php
                    }
                    ?>
                    <ul class="nav nav-pills flex-column">
                        <?php
                        if (!empty($currentCat->id)) {
                            ?>
                            <li class="nav-item">
                                <a class="nav-link" href="/admin/components"><i class="fa fa-times-circle"></i> Clear Filters</a>
                            </li>
                            <?php
                        }

                        foreach ($categories as $category) {
                            ?>
                            <li class="nav-item">
                                <a class="nav-link<?=($category->id == $currentCat->id) ? ' active' : ''?>" href="/admin/components/<?=$category->id?>"><?=$category->name?></a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php
}

?>
