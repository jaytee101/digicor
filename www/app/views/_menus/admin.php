<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item<?=$viewOpts['menu']['section'] == 'home' ? ' active' : '' ?>">
                    <a class="nav-link" href="/admin">HOME <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item<?=$viewOpts['menu']['section'] == 'components' ? ' active' : '' ?>">
                    <a class="nav-link" href="/admin/components">COMPONENTS</a>
                </li>
                <li class="nav-item<?=$viewOpts['menu']['section'] == 'configurations' ? ' active' : '' ?>">
                    <a class="nav-link" href="/admin/configurations">CONFIGURATIONS</a>
                </li>
                <li class="nav-item<?=$viewOpts['menu']['section'] == 'categories' ? ' active' : '' ?>">
                    <a class="nav-link" href="/admin/categories">CATEGORIES</a>
                </li>
                <li class="nav-item<?=$viewOpts['menu']['section'] == 'customers' ? ' active' : '' ?>">
                    <a class="nav-link" href="/admin/customers">CUSTOMERS</a>
                </li>
                <li class="nav-item<?=$viewOpts['menu']['section'] == 'users' ? ' active' : '' ?>">
                    <a class="nav-link" href="/admin/users">USERS</a>
                </li>
            </ul>

            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="/" class="nav-link">FRONT-END</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link" data-toggle="modal" data-target="#logout">LOG OUT</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
