<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="<?=APP_URL?>"><img src="/public/img/logo.png" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item dropdown<?=$viewOpts['menu']['section'][0] == 'games' ? ' active' : ''?>">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true" id="activeCurrency">
                    <?php
                    if ($_SESSION['user']['location']['country'] == 'NZ') {
                        ?>
                        <img src="/public/img/nz.svg" alt="" style="height: 16px; "> NZ
                        <?php
                    } else {
                        ?>
                        <img src="/public/img/au.svg" alt="" style="height: 16px; "> AU
                        <?php
                    }
                    ?>
                    </a>
                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <a class="dropdown-item" id="setAUD"><img src="/public/img/au.svg" alt="" style="height: 25px; "> AU</a>
                        <a class="dropdown-item" id="setNZD"><img src="/public/img/nz.svg" alt="" style="height: 25px; "> NZ</a>
                    </div>
                </li>
                <li class="nav-item<?=$viewOpts['menu']['section'] == 'home' ? ' active' : '' ?>">
                    <a class="nav-link" href="/">HOME <span class="sr-only">(current)</span></a>
                </li>
            </ul>

            <ul class="navbar-nav ml-auto">
                <?php
                if (empty($_SESSION['authenticated'])) {
                    ?>
                    <li class="nav-item">
                        <a href="#" class="nav-link" data-toggle="modal" data-target="#createAccount">CREATE ACCOUNT</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link" data-toggle="modal" data-target="#login">LOG IN</a>
                    </li>
                    <?php
                } else {
                    if ($_SESSION['user']['role'] == 'staff' || $_SESSION['owner'] == true) {
                        ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin">ADMIN</a>
                        </li>
                        <?php
                    }
                    ?>
                    <li class="nav-item">
                        <a href="#" class="nav-link" data-toggle="modal" data-target="#logout">LOG OUT</a>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>
    </div>
</nav>

<form action="<?=APP_URL?>" method="post" style="display: none; " id="setCurrency">
    <input type="hidden" name="action" value="setCurrency">
    <input type="hidden" name="currency" id="currency">
    <input type="hidden" name="redirect" value="<?=$_SERVER['REQUEST_URI']?>">
</form>

<script>
$('#setAUD').click(function() {
    console.log('set currency to AUD clicked');
    $('#currency').val('AUD');
    $('#setCurrency').submit();

    // display active currency
    $('#activeCurrency').html('<img src="/public/img/au.svg" alt="" style="height: 16px; "> AU');
});

$('#setNZD').click(function() {
    console.log('set currency to NZD clicked');
    $('#currency').val('NZD');
    $('#setCurrency').submit();

    // display active currency
    $('#activeCurrency').html('<img src="/public/img/nz.svg" alt="" style="height: 16px; "> NZ');
});
</script>
