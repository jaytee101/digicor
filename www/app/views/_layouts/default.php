<?php

include_once(views . '/_blocks/head.php');

// menu
if ($viewOpts['menu']['enabled'] == true) {
    include_once(views . '/_menus/' . $viewOpts['menu']['content'] . '.php');
}

?>

<div id="alerts" style="z-index: 999; ">
<?php
if (!empty($_SESSION['flash'])) {
    ?>
    <div class="alert alert-<?=$_SESSION['flash']['style']?> alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fas fa-times-circle"></i></span></button>
        <?=$_SESSION['flash']['message']?>
    </div>
    <?php

    // clear the session's flash messaging
    $_SESSION['flash'] = [];
}
?>
</div>

<div style="margin-bottom: 1rem; "></div>

<?php

// opening full/width
if ($viewOpts['page']['fullwidth'] == true) {
    echo '<div class="container-fluid">';
} else {
    echo '<div class="container">';
}

if ($viewOpts['sidebar']['enabled'] == true) {
    ?>
    <div class="row">
        <div class="col-md-3 col-lg-2">
        <?php
        include_once(views . '/_sidebars/' . $viewOpts['sidebar']['content'] . '.php');
        ?>
        </div>

        <div class="col-md-9 col-lg-10">
        <?php
        include_once(views . '/content/' . $viewOpts['page']['content'] . '.php');
        ?>
        </div>
    </div>
    <?php
} else {
    ?>
    <div class="row">
        <div class="col">
        <?php
        include_once(views . '/content/' . $viewOpts['page']['content'] . '.php');
        ?>
        </div>
    </div>
    <?php
}

// closing full/width
echo '</div>';

if (!empty($_SESSION['authenticated'])) {
    include_once(views . '/_modals/user.logout.php');
} else {
    include_once(views . '/_modals/user.login.php');
    include_once(views . '/_modals/user.create.php');
}

if ($viewOpts['footer']['enabled'] == true) {
    include_once(views . '/_blocks/foot.php');
}
