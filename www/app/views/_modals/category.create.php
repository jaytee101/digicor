<!-- Create New Category Modals -->
<div class="modal fade" id="createCategory" tabindex="-1" role="dialog" aria-labelledby="createCategoryLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="/admin/categories">
                <div class="modal-header">
                    <h5 class="modal-title" id="createCategoryLabel">Create a New Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group row">
                        <label for="categoryName" class="col-sm-3">Name</label>
                        <div class="col-sm-9">
                            <input type="text" id="categoryName" class="form-control" name="name" placeholder="Name of the Category">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="categoryDescription" class="col-sm-3">Description</label>
                        <div class="col-sm-9">
                            <input type="text" id="categoryDescription" class="form-control" name="description" placeholder="Description of the Category">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="hidden" name="action" value="create">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-success">Create Category</button>
                </div>
            </form>
        </div>
    </div>
</div>
