<!-- Log In Modals -->
<div class="modal fade" id="createAccount" tabindex="-1" role="dialog" aria-labelledby="createAccountForm" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form class="form-horizontal" method="POST" action="/">
                <div class="modal-header">
                    <h5 class="modal-title" id="createAccountForm">Create an Account</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group row">
                        <label for="name" class="control-label col-md-3 col-form-label text-right">Name</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="firstname" placeholder="First Name" />
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="lastname" placeholder="Last Name" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="control-label col-md-3 col-form-label text-right">Email Address*</label>
                        <div class="col">
                            <input type="text" class="form-control" name="email" placeholder="Your Email Address (needs to be valid for verification!)" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="company" class="control-label col-md-3 col-form-label text-right">Company</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="company" name="company" placeholder="Company or Organization Name" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="contact" class="control-label col-md-3 col-form-label text-right">Contact Number</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="contact" name="contact" placeholder="Best Contact Number" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="login" class="control-label col-md-3 col-form-label text-right">Password*</label>
                        <div class="col-md-4">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="login" class="control-label col-md-3 col-form-label text-right">Confirm Password*</label>
                        <div class="col-md-4">
                            <input type="password" class="form-control" id="confirm" name="confirm" placeholder="Confirm Password" />
                            <div id="pwStatus"></div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="hidden" name="action" value="create">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-success createButton" disabled="disabled">Create a New Account</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
function pwCheck(){
    var pw1 = $('#password').val();
    var pw2 = $('#confirm').val();

    if(pw1 != pw2) {
        $('#pwStatus').text('');
        $('#pwStatus').removeClass('text-success').addClass('text-danger');
        $('#pwStatus').text("Passwords don't match!");

        $('.createButton').attr("disabled", "disabled");
    } else {
        $('#pwStatus').text('');
        $('#pwStatus').removeClass('text-danger').addClass('text-success');
        $('#pwStatus').text("Passwords Match!")

        $('.createButton').removeAttr("disabled");
    }

    if ((pw1 == '') && (pw2 == '')) {
        $('.createButton').attr("disabled", "disabled");
        $('#pwStatus').text('');
    }
}

$(document).ready(function() {
    $('#password').keyup(function() {
        pwCheck();
    });

    $('#confirm').keyup(function() {
        pwCheck();
    });
});
</script>
