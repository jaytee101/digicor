<!-- MODAL: New Component List -->
<div class="modal fade" id="importNewComponent" tabindex="-1" role="dialog" aria-labelledby="importNewComponentForm" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="/admin/components/import/new" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="importNewComponentForm">Import a List of New Components</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <input type="file" name="upload" id="" class="form-control-file">
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="hidden" name="action" value="upload">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary">Upload File</button>
                </div>
            </form>
        </div>
    </div>
</div>