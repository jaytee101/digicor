<!-- Log In Modals -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="loginForm" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" method="POST" action="/">
                <div class="modal-header">
                    <h5 class="modal-title" id="loginForm">Log In</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group row">
                        <label for="login" class="control-label col-3 col-form-label text-right">Log In</label>
                        <div class="col">
                            <input type="text" class="form-control" name="login" placeholder="Email" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="login" class="control-label col-3 col-form-label text-right">Password</label>
                        <div class="col">
                            <input type="password" class="form-control" name="password" placeholder="Password" />
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="hidden" name="action" value="login">
                    <input type="hidden" name="redirect" value="<?=!empty($_SESSION['REQUEST_URI']) ? $_SESSION['REQUEST_URI'] : '/'?>">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary">Log In</button>
                </div>
            </form>
        </div>
    </div>
</div>