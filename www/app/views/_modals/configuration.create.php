<!-- Create New Configuration Modal -->
<div class="modal fade" id="createConfig" tabindex="-1" role="dialog" aria-labelledby="createConfigLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="/admin/configurations">
                <div class="modal-header">
                    <h5 class="modal-title" id="createConfigLabel">Create a New Configuration</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group row">
                        <label for="configName" class="col-sm-3">Name</label>
                        <div class="col-sm-9">
                            <input type="text" id="configName" class="form-control" name="name" placeholder="Name of the Configuration">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="configDescription" class="col-sm-3">Description</label>
                        <div class="col-sm-9">
                            <input type="text" id="configDescription" class="form-control" name="description" placeholder="Description of the Configuration">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="hidden" name="action" value="create">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary">Create Configuration</button>
                </div>
            </form>
        </div>
    </div>
</div>
