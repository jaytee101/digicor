<!-- Log Out Modals -->
<div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="logoutForm" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" method="POST" action="/">
                <div class="modal-header">
                    <h5 class="modal-title" id="logoutForm">Log Out</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    Are you sure you want to log out?
                </div>

                <div class="modal-footer">
                    <input type="hidden" name="action" value="logout">
                    <input type="hidden" name="user" value="<?=$_SESSION['user']['uuid']?>">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary">Log Out</button>
                </div>
            </form>
        </div>
    </div>
</div>
