<?php

session_start();

// root constant
define('ROOT', dirname(__FILE__));

// file location constants
define('ctrlr', ROOT.'/app/controllers');
define('model', ROOT.'/app/models');
define('views', ROOT.'/app/views');

require 'vendor/autoload.php';
require ROOT.'/config.php';
require ROOT.'/R.php';

R::setup(DB_TYPE.':host='.DB_HOST.';port='.DB_PORT.';dbname='.DB_NAME, DB_USER, DB_PASS);
// echo R::testConnection() ? 'connected to the DB' : 'not connected to the DB'; die();

R::ext('xdispense', function ($type) {
    return R::getRedBean()->dispense($type);
});

$base = new \app\Core\Base();

// SETUP error logging
if ($_SERVER['HTTP_HOST'] == 'digicor.local') {
    error_reporting(E_ERROR | E_WARNING | E_PARSE);
}

ini_set('log_errors', 1);
ini_set('display_errors', 1);

// SITE TIMEZONE
date_default_timezone_set(TIMEZONE);

include ROOT . '/bootstrap.php';
include ROOT . '/router.php';

R::close();

// end of file
