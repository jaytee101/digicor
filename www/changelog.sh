#!/bin/bash

changelog=$(git log --pretty=format:'<commit>%h|%an|%s|%b|%d|%ct' --abbrev-commit)
echo ${changelog} > 'gitlog'
