<?php

$base->get("/admin/components/create", function () {
    $controller = new app\Controllers\Components;
    return $controller->{'create'}();
});

$base->get("/admin/components/disabled", function () {
    $controller = new app\Controllers\Components;
    return $controller->{'admin'}(true);
});

$base->get("/admin/components/import", function () {
    $controller = new app\Controllers\Components;
    return $controller->{'import'}();
});

$base->get("/admin/components/replace", function () {
    $controller = new app\Controllers\Components;
    return $controller->{'replace'}();
});

$base->get("/admin/components/:id", function ($id) {
    $controller = new app\Controllers\Components;
    return $controller->{'byCategory'}($id);
});

$base->get("/admin/components", function () {
    $controller = new app\Controllers\Components;
    return $controller->{'admin'}();
});

/**
 * POST REQUESTS BELOW
 */

$base->post("/admin/components/import", function () {
    $controller = new app\Controllers\Components;
    return $controller->{'import'}();
});

$base->post("/admin/components/category/:id", function ($id) {
    $controller = new app\Controllers\Components;
    return $controller->{'ajaxCategoryComponents'}($id);
});

$base->post("/admin/components/view/:id", function ($id) {
    $controller = new app\Controllers\Components;
    return $controller->{'ajaxView'}($id);
});

$base->post("/admin/components", function () {
    $controller = new app\Controllers\Components;
    return $controller->{'admin'}();
});
