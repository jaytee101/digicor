<?php

// GET /changelog
$base->get("/changelog", function () {
    $controller = new app\Controllers\Home;
    return $controller->{'changelog'}();
});

// GET /
$base->get("/", function () {
    $controller = new app\Controllers\Home;
    return $controller->{'index'}();
});

// POST /
$base->post("/", function () {
    $controller = new app\Controllers\Home;
    return $controller->{'index'}();
});
