<?php

$base->get("/admin/configurations/:config/sections/:section/edit", function ($config, $section) {
    $controller = new app\Controllers\Configurations;
    return $controller->{'editSection'}($config, $section);
});

$base->get("/admin/configurations/:id/sections", function ($id) {
    $controller = new app\Controllers\Configurations;
    return $controller->{'manageSections'}($id);
});

$base->get("/admin/configurations/disabled", function () {
    $controller = new app\Controllers\Configurations;
    return $controller->{'admin'}(true);
});

$base->get("/admin/configurations/create", function () {
    $controller = new app\Controllers\Configurations;
    return $controller->{'create'}();
});

$base->get("/admin/configurations/:id", function ($id) {
    $controller = new app\Controllers\Configurations;
    return $controller->{'manage'}($id);
});

$base->get("/admin/configurations", function () {
    $controller = new app\Controllers\Configurations;
    return $controller->{'admin'}();
});

//
// POST Requests
//
$base->post("/admin/configurations/:config/sections/:section/edit", function ($config, $section) {
    $controller = new app\Controllers\Configurations;
    return $controller->{'editSection'}($config, $section);
});

$base->post("/admin/configurations/:id/sections", function ($id) {
    $controller = new app\Controllers\Configurations;
    return $controller->{'manageSections'}($id);
});

$base->post("/admin/configurations/create", function () {
    $controller = new app\Controllers\Configurations;
    return $controller->{'create'}();
});

$base->post("/admin/configurations/:id", function ($id) {
    $controller = new app\Controllers\Configurations;
    return $controller->{'manage'}($id);
});

$base->post("/admin/configurations", function () {
    $controller = new app\Controllers\Configurations;
    return $controller->{'admin'}();
});
