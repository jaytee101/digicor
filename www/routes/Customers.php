<?php

// GET /admin/customers
$base->get("/admin/customers", function () {
    $controller = new app\Controllers\Customers;
    return $controller->{'admin'}();
});

// POST /admin/customers
$base->post("/admin/customers", function () {
    $controller = new app\Controllers\Customers;
    return $controller->{'admin'}();
});
