<?php

// GET /admin/users
$base->get("/admin/users", function () {
    $controller = new app\Controllers\Users;
    return $controller->{'admin'}();
});

// POST /admin/users
$base->post("/admin/users", function () {
    $controller = new app\Controllers\Users;
    return $controller->{'admin'}();
});
