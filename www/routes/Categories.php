<?php

// GET /
$base->get("/admin/categories", function () {
    $controller = new app\Controllers\Categories;
    return $controller->{'admin'}();
});

// POST /
$base->post("/admin/categories", function () {
    $controller = new app\Controllers\Categories;
    return $controller->{'admin'}();
});
