<?php

// GET /quotes/:id
$base->get("/quotes/:id", function ($id) {
    $controller = new app\Controllers\Quotes;
    return $controller->{'view'}($id);
});

// POST /quotes/:id
$base->post("/quotes/:id", function ($id) {
    $controller = new app\Controllers\Quotes;
    return $controller->{'view'}($id);
});

