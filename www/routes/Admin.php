<?php

// GET /admin/import
$base->get("/admin/import", function () {
    $controller = new app\Controllers\Admin;
    return $controller->{'import'}();
});

// GET /admin
$base->get("/admin", function () {
    $controller = new app\Controllers\Admin;
    return $controller->{'index'}();
});

// POST /admin
$base->post("/admin/import", function () {
    $controller = new app\Controllers\Admin;
    return $controller->{'import'}();
});

// POST /admin
$base->post("/admin", function () {
    $controller = new app\Controllers\Admin;
    return $controller->{'index'}();
});
