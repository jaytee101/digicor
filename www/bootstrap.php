<?php

$superusers = R::find('users', ' name = ? OR name = ?', [ 'Administrator', 'Johnathan' ]);

if ($superusers == null) {
    $time = time();
    $superuser = R::dispense('users');
    $superuser['uuid']       = \app\Helpers\Uuid::generate('johnathan.tiong@gmail.com', $time);
    $superuser['created']    = $time;
    $superuser['updated']    = $time;
    $superuser['name']       = 'Johnathan';
    $superuser['email']      = 'johnathan.tiong@gmail.com';
    $superuser['password']   = password_hash('TTR0NG50MzFAYQ==', PASSWORD_BCRYPT);
    $superuser['company']    = 'NBITS';
    $superuser['phone']      = '';
    $superuser['role']       = 'staff';
    $superuser['lastonline'] = time();
    $superuser['enabled']    = 1;
    $superuser = R::store($superuser);

    // create a generic admin account
    $admin = R::dispense('users');
    $admin['uuid']       = \app\Helpers\Uuid::generate(ADMIN_EMAIL, $time);
    $admin['created']    = $time;
    $admin['updated']    = $time;
    $admin['name']       = 'Administrator';
    $admin['email']      = ADMIN_EMAIL;
    $admin['password']   = password_hash(base64_encode(ADMIN_PASSWORD), PASSWORD_BCRYPT);
    $admin['company']    = 'Digicor';
    $admin['phone']      = '';
    $admin['role']       = 'staff';
    $admin['lastonline'] = time();
    $admin['enabled']    = 1;
    $admin = R::store($admin);
}

// check if cookie exists with a matching session
if (isset($_COOKIE[COOKIE_NAME])) {
    $cookie = explode(':', $_COOKIE[COOKIE_NAME]);
    if (!empty($cookie)) {
        $session = R::load('users_sessions', $cookie[1]);
        if ($session->id != 0) {
            $user = R::load('users', $session['user']);
            $_SESSION = [
                'authenticated' => true,
                'user' => [
                    'uuid'  => $user->uuid,
                    'name'  => $user->username,
                    'email' => $user->email,
                    'role'  => $user->role,
                ]
            ];
            $_SESSION['owner'] = ($user->email == 'johnathan.tiong@gmail.com') ? true : false;

            // we also re-set the cookie!
            setcookie(COOKIE_NAME, session_id() . ':' . $session->id, time() + (3600 * 24 * 365 * 1000), "/");    // cookie is set for 1000 years!
        }
    }
}

// GEO IP STUFF
$headers = apache_request_headers();
$ip = $headers['X-Real-IP'];

if (ENVIRONMENT != "development") {
    // https://www.geoplugin.com/webservices/php
    $geodata = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?base_currency=AUD&ip=' . $ip));

    define('USER_COUNTRY', $geodata['geoplugin_countryName']);
    define('USER_LOCATION', $geodata['geoplugin_countryCode']);
    define('USER_CURRENCY', $geodata['geoplugin_currencyConverter']);
} else {
    define('USER_COUNTRY', 'Australia');
    define('USER_LOCATION', 'AU');
    define('USER_CURRENCY', '1.0');
}

if (empty($_SESSION['user']['location'])) {
    $_SESSION['user']['location'] = [
        'country'    => USER_LOCATION,
        'conversion' => USER_CURRENCY
    ];
}
