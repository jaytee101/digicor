$(document).ready(function() {
    // flex datalists
    $('.flexlist').flexdatalist({
        minLength: 1
    });

    // base datatables
    $('.datatable').DataTable({
        autoWidth: false,
    });

    // tooltips
    $('[rel="tooltip"]').tooltip({
        html: true,
    });

    // popovers
    $(function () {
        $('[data-toggle="popover"]').popover({
            trigger: 'focus',
            html: true
        })
    });
});
