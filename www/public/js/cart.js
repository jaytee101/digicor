
/**
 * removes a product from the cart array pre-updateCart
 *
 * @param {int} product
 * @param {array} myCart
 */
function removeProduct(product, myCart) {
    //
}

/**
 * update the cart
 *
 * @param {array} myCart
 */
function updateCart(params) {
    var configuration = params['configId'];

    $.ajax({
        type: "POST",
        url: "/quotes/" + configuration,
        dataType: "json",
        data: {
            action: 'ajax',
            call: 'cartUpdate',
            data: params,
        },
        success: function(result) {
            $.each(result['itemcounts'], function (key, value) {
                var itemlimit = $('#' + key).data('limit');
                $('#' + key).html('<p>' + value + '/' + itemlimit + ' selected</p>');

                if (value > itemlimit) {
                    $('#' + key).append('<b class="text-danger">Item Limit Exceeded!</b>');
                }
            });

            $('#cartList').html(result['cartList']);

            // update total price
            $('#totalPrice').html(parseFloat((result['totalprice']) / 100).toFixed(2));
        },
        error: function(result) {
            alert('Error: Cart Update failed');
        }
    });
}
