#!/bin/bash

old=$(cat version)
echo "[digicor-configurator] [old] $(echo ${old})"

latesttag=$(git describe --tags)
echo ${latesttag} > 'version'

new=$(cat version)
echo "[digicor-configurator] [new] $(echo ${new})"
