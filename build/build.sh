#!/bin/bash

echo "Deploy script started"

cd /srv/sites/digicor.jtiong.com

tag=$(date +"%Y.%m.%d.%H.%M.%S")
git tag ${tag}
git push origin ${tag}

git fetch
git reset --hard origin/master

echo "Updating changelog"

git log --pretty=format:'<commit>%h|%an|%s|%b|%d|%ct' --abbrev-commit > 'www/gitlog'

echo "Updating version numbering"

old=$(cat version)
echo "[digicor-configurator] [old] $(echo ${old})"

git describe --tags > 'www/version'

new=$(cat version)
echo "[digicor-configurator] [new] $(echo ${new})"


echo "Deploy script finished"